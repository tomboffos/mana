FROM php:8.1-fpm

# Fix debconf warnings upon .build
ENV PHP_INI_DIR /usr/local/etc/php
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
  locales \
  libzip-dev \
  zip \
  unzip \
  p7zip-full \
  libmcrypt-dev \
  libpng-dev \
  libjpeg-dev \
  libgmp-dev \
  nginx

RUN pecl install mcrypt-1.0.5  \
  && docker-php-ext-enable mcrypt \
  && docker-php-ext-configure gd --with-jpeg \
  && docker-php-ext-install zip opcache mysqli pdo pdo_mysql gd gmp bcmath


# Install PHP Redis extension
RUN pecl install -o -f redis \
  &&  rm -rf /tmp/pear \
  &&  docker-php-ext-enable redis

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
  locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

COPY infrastructure/php-fpm/www.conf /usr/local/etc/php-fpm.d
COPY infrastructure/php-fpm/opcache.ini "$PHP_INI_DIR"/conf.d
COPY infrastructure/php-fpm/php.ini "$PHP_INI_DIR"/conf.d

WORKDIR "/application"

COPY . /application

RUN composer install --ignore-platform-reqs --no-scripts

RUN chmod +x /application/run.sh

EXPOSE 9000 80

CMD ["/application/run.sh"]
