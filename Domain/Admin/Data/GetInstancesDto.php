<?php

namespace Domain\Admin\Data;

class GetInstancesDto
{
    public function __construct(
        public readonly ?string $name,
        public readonly ?bool $is_active
    )
    {

    }
}
