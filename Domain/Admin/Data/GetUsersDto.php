<?php

namespace Domain\Admin\Data;

class GetUsersDto
{
    public function __construct(
        public readonly ?string $name,
        public readonly ?bool $status,
        public readonly ?string $email,
    )
    {

    }
}
