<?php

namespace Domain\Admin\Exceptions;

use Domain\Shared\Exceptions\DomainException;
use Throwable;

class InstancesNotFoundException extends DomainException
{
    public function __construct(string $message = "", int $code = 404, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getErrorAlias(): string
    {
        return 'instances-not-found';
    }
}
