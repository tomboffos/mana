<?php

declare(strict_types=1);

namespace Domain\Auth\Contracts;

use Domain\Auth\Data\ChangePasswordDto;
use Domain\Auth\Data\LoggedInUserDto;
use Domain\Auth\Data\LoginUserDto;
use Domain\Auth\Data\RegisterUserDto;
use Domain\User\Models\User;

interface AuthServiceInterface
{
    public function register(RegisterUserDto $dto): LoggedInUserDto;

    public function login(LoginUserDto $dto): LoggedInUserDto;

    public function changePassword(ChangePasswordDto $dto): User;

    public function loginAdmin(LoginUserDto $dto) : LoggedInUserDto;
}
