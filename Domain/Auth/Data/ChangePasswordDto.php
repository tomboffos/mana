<?php

declare(strict_types=1);

namespace Domain\Auth\Data;

use Domain\User\Models\User;

class ChangePasswordDto
{
    public function __construct(
        public readonly User $user,
        public readonly string $oldPassword,
        public readonly string $newPassword,
    ) {
    }
}
