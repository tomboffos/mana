<?php

declare(strict_types=1);

namespace Domain\Auth\Data;

use Domain\User\Models\User;
use Laravel\Sanctum\NewAccessToken;

class LoggedInUserDto
{
    public function __construct(
        public readonly User $user,
        public readonly NewAccessToken $newAccessToken,
    ) {
    }
}
