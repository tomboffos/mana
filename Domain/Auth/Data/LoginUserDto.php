<?php

declare(strict_types=1);

namespace Domain\Auth\Data;


class LoginUserDto
{
    public function __construct(
        public readonly string $email,
        public readonly string $password,
    ) {
    }
}
