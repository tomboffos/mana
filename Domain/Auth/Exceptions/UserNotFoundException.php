<?php

declare(strict_types=1);

namespace Domain\Auth\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class UserNotFoundException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'user-not-found';
    }
}
