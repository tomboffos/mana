<?php

declare(strict_types=1);

namespace Domain\Auth\Exceptions;

use Domain\Shared\Exceptions\DomainException;
use Throwable;

class WrongPasswordException extends DomainException
{
    public function __construct(string $message = "", int $code = 401, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getErrorAlias(): string
    {
        return 'wrong-password-exception';
    }
}
