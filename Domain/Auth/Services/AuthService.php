<?php

declare(strict_types=1);

namespace Domain\Auth\Services;

use Domain\Auth\Contracts\AuthServiceInterface;
use Domain\Auth\Data\ChangePasswordDto;
use Domain\Auth\Data\LoggedInUserDto;
use Domain\Auth\Data\LoginUserDto;
use Domain\Auth\Data\RegisterUserDto;
use Domain\Auth\Exceptions\UserAlreadyExistsException;
use Domain\Auth\Exceptions\UserNotFoundException;
use Domain\Auth\Exceptions\WrongPasswordException;
use Domain\Auth\Mail\ResetPasswordMail;
use Domain\Company\Contracts\CompanyServiceInterface;
use Domain\User\Contracts\UserServiceInterface;
use Domain\User\Data\CreateUserDto;
use Domain\User\Models\PasswordReset;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Illuminate\Database\DatabaseManager;
use Illuminate\Hashing\HashManager;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Throwable;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;

class AuthService implements AuthServiceInterface
{
    private const TOKEN_NAME = 'api-token';

    public function __construct(
        private readonly UserServiceInterface    $userService,
        private readonly CompanyServiceInterface $companyService,
        private readonly HashManager             $hashManager,
        private readonly DatabaseManager         $databaseManager,
    )
    {
    }

    /**
     * @throws Throwable
     */
    public function register(RegisterUserDto $dto): LoggedInUserDto
    {
        return $this->databaseManager->transaction(function () use ($dto) {
            $user = $this->userService->findByEmail($dto->email);
            if (!is_null($user)) {
                throw new UserAlreadyExistsException();
            }

            $company = $this->companyService->create();
            $user = $this->userService->create(new CreateUserDto(
                $dto->name,
                $dto->email,
                $this->hashManager->make($dto->password),
                $company->id,
                RBAC::CUSTOMER,
            ));

            return new LoggedInUserDto(
                $user,
                $user->createToken(self::TOKEN_NAME)
            );
        });
    }

    public function login(LoginUserDto $dto): LoggedInUserDto
    {
        $user = $this->userService->findByEmail($dto->email);
        if (is_null($user)) {
            throw new UserNotFoundException();
        }

        $this->checkPassword($user, $dto->password);

        return new LoggedInUserDto(
            $user,
            $user->createToken(self::TOKEN_NAME)
        );
    }

    private function checkPassword(User $user, string $password): void
    {
        $checkPassword = $this->hashManager->check($password, $user->password);
        if (!$checkPassword) {
            throw new WrongPasswordException();
        }
    }

    public function changePassword(ChangePasswordDto $dto): User
    {
        $user = $dto->user;
        $checkPassword = $this->hashManager->check($dto->oldPassword, $user->password);
        if (!$checkPassword) {
            throw new WrongPasswordException();
        }

        $user->password = $this->hashManager->make($dto->newPassword);
        $this->userService->save($user);

        return $user;
    }

    public function loginAdmin(LoginUserDto $dto): LoggedInUserDto
    {
        $user = $this->userService->findByEmail($dto->email);

        if (is_null($user) || $user->role_id != RBAC::ADMIN)
            throw  new UserNotFoundException();

        return new LoggedInUserDto(
            $user,
            $user->createToken(self::TOKEN_NAME)
        );

    }
}
