<?php

declare(strict_types=1);

namespace Domain\Company\Contracts;

use Domain\Company\Models\Company;

interface CompanyServiceInterface
{
    public function create(): Company;
}
