<?php

declare(strict_types=1);

namespace Domain\Company\Models;

use Domain\Instance\Models\Instance;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property ?Instance $instance
 * @property-read User $user
 */
class Company extends Model
{
    protected $table = 'companies';

    public function instance(): HasOne
    {
        return $this->hasOne(Instance::class);
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'company_id');
    }
}
