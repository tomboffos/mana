<?php

declare(strict_types=1);

namespace Domain\Company\Repositories;

use Domain\Company\Models\Company;

interface CompanyRepositoryInterface
{
    public function save(Company $company): void;
}
