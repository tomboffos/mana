<?php

declare(strict_types=1);

namespace Domain\Company\Services;

use Domain\Company\Contracts\CompanyServiceInterface;
use Domain\Company\Models\Company;
use Domain\Company\Repositories\CompanyRepositoryInterface;

class CompanyService implements CompanyServiceInterface
{
    public function __construct(
        private readonly CompanyRepositoryInterface $companyRepository,
    ) {
    }

    public function create(): Company
    {
        $company = new Company();
        $this->companyRepository->save($company);

        return $company;
    }
}
