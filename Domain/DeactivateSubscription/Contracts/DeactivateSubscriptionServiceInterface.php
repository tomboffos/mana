<?php

declare(strict_types=1);

namespace Domain\DeactivateSubscription\Contracts;

use Illuminate\Database\Eloquent\Collection;
interface DeactivateSubscriptionServiceInterface
{
    public function deactivate(?Collection $subscriptions): void;
}
