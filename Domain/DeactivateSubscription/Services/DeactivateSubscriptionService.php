<?php

declare(strict_types=1);

namespace Domain\DeactivateSubscription\Services;

use Domain\DeactivateSubscription\Contracts\DeactivateSubscriptionServiceInterface;
use Domain\Subscription\Mail\SubscriptionExpiredMail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class DeactivateSubscriptionService implements DeactivateSubscriptionServiceInterface
{


    public function __construct()
    {
    }

    /**
     */
    public function deactivate(?Collection $subscriptions): void
    {
        
        if ($subscriptions) {
            $subscriptions->each(function ($subscription) {
                $subscription->is_active = false;
                $subscription->instance->is_active = false;
                $subscription->instance->host->instance_id = null;

                $validUntil = $subscription->valid_until;
                $user = $subscription->instance->company->first()->user()->first();
                $mail = new SubscriptionExpiredMail($validUntil);
                Mail::to($user)->bcc($mail);

                $subscription->save();
                $subscription->instance->host->save();
                $subscription->instance->save();
            });
        }
    }
}
