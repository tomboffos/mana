<?php

declare(strict_types=1);

namespace Domain\Feature\Contracts;

use Domain\Feature\Data\CreateFeatureDto;
use Domain\Feature\Data\UpdateFeatureDto;
use Domain\Feature\Models\Feature;
use Illuminate\Database\Eloquent\Collection;

interface FeatureServiceInterface
{
    public function getAll(): Collection;

    public function list(): array;

    public function calculatePrices(array $features, $validFrom, $validUntil): float;

    public function create(CreateFeatureDto $dto): Feature;

    public function update(int $id, UpdateFeatureDto $dto): ?Feature;

    public function delete(int $id): void;
}
