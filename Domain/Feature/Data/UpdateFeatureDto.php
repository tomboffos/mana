<?php

namespace Domain\Feature\Data;

class UpdateFeatureDto
{
    public function __construct(
        public readonly string $name,
        public readonly string $alias,
        public readonly int    $price,
        public readonly bool   $active,
    )
    {

    }


    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'alias' => $this->alias,
            'price' => $this->price,
            'active' => $this->active,
        ];
    }
}
