<?php

namespace Domain\Feature\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $alias
 * @property int $price
 * @property boolean $active
 */
class Feature extends Model
{
    protected $table = 'features';


    protected $fillable = [
        'name',
        'alias',
        'price',
        'active',
    ];
}
