<?php

namespace Domain\Feature\Repository;

use Domain\Feature\Data\CreateFeatureDto;
use Domain\Feature\Data\UpdateFeatureDto;
use Domain\Feature\Models\Feature;
use Illuminate\Database\Eloquent\Collection;

interface FeatureRepositoryInterface
{
    public function getAll(): Collection;

    public function create(CreateFeatureDto $dto): Feature;

    public function update(Feature $feature, UpdateFeatureDto $dto): Feature;

    public function delete(Feature $feature): void;

    public function findByAlias(string $alias): ?Feature;

    public function findById(int $id) : ?Feature;
}
