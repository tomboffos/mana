<?php

declare(strict_types=1);

namespace Domain\Feature\Services;

use DateTime;
use Domain\Feature\Contracts\FeatureServiceInterface;
use Domain\Feature\Data\CreateFeatureDto;
use Domain\Feature\Data\UpdateFeatureDto;
use Domain\Feature\Models\Feature;
use Domain\Feature\Repository\FeatureRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FeatureService implements FeatureServiceInterface
{
    public function __construct(
        public readonly FeatureRepositoryInterface $featureRepository
    ) {
    }
    private const DAYS_IN_MONTH = 30;
    public const FEATURE_LIST = [
        'botTrader' => [
            'name' => 'Bot trader',
            'price' => 25.0
        ],
        'builder' => [
            'name' => 'Page builder',
            'price' => 25.0
        ],
        'eco' => [
            'name' => 'Ecosystem',
            'price' => 25.0
        ],
        'forex' => [
            'name' => 'Forex & Investment',
            'price' => 25.0
        ],
        'ico' => [
            'name' => 'Token ICO',
            'price' => 25.0
        ],
        'knowledge' => [
            'name' => 'Knowledge base',
            'price' => 25.0
        ],
        'livechat' => [
            'name' => 'LiveChat',
            'price' => 25.0
        ],
        'mlm' => [
            'name' => 'MLM',
            'price' => 25.0
        ],
        'staking' => [
            'name' => 'Staking Crypto',
            'price' => 25.0
        ],
        'walletConnect' => [
            'name' => 'Wallet Connect',
            'price' => 25.0
        ],
    ];


    public function list(): array
    {
        $featureList = [];

        foreach ($this->featureRepository->getAll() as $feature)
            $featureList[$feature->alias] = $feature;


        return $featureList;
    }

    public function calculatePrices(array $features, $validFrom, $validUntil): float
    {
        $featurePrice = 0;
        foreach ($features as $feature) {
            $featureModel = $this->featureRepository->findByAlias($feature);
            if (!is_null($featureModel))
                $featurePrice += $featureModel->price;
        }

        $pricePerDay = $featurePrice / self::DAYS_IN_MONTH;
        $daysNeeded = date_diff($validFrom, $validUntil)->days;
        $price = ceil($daysNeeded * $pricePerDay);

        return $price;
    }

    public function create(CreateFeatureDto $dto): Feature
    {
        return $this->featureRepository->create($dto);
    }

    public function update(int $id, UpdateFeatureDto $dto): ?Feature
    {
        $feature = $this->featureRepository->findById($id);

        if (!is_null($feature))
            return $this->featureRepository->update($feature, $dto);

        return null;
    }

    public function delete(int $id): void
    {
        $feature = $this->featureRepository->findById($id);

        if ($feature)
            $this->featureRepository->delete($feature);
    }

    public function getAll(): Collection
    {
        return $this->featureRepository->getAll();
    }
}
