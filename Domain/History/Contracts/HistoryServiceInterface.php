<?php

declare(strict_types=1);

namespace Domain\History\Contracts;

use Domain\History\Data\CreateHistoryDto;
use Domain\History\Models\History;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface HistoryServiceInterface
{
    public function create(CreateHistoryDto $dto): History;
    public function getHistoryByUser(int $user);
    public function getLastVisitByUser(int $userId);
}
