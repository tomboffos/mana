<?php

declare(strict_types=1);

namespace Domain\History\Data;

class CreateHistoryDto
{
    public function __construct(
        public readonly int $user_id,
        public readonly string $gadget_type,
        public readonly string $ip_address,
        public readonly string $city,
        public readonly string $country,
        public readonly \DateTime $last_visit,
    ) {
    }
}
