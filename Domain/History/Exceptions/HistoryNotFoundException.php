<?php

declare(strict_types=1);

namespace Domain\History\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class HistoryNotFoundException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'history-not-found';
    }
}
