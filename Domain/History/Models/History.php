<?php

declare(strict_types=1);

namespace Domain\History\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $gadget_type
 * @property string $ip_address
 * @property string $city
 * @property string $country
 * @property \DateTime $last_visit
 */
class History extends Model
{
    protected $table = 'histories';
    protected $fillable = [
        'user_id',
        'gadget_type',
        'ip_address',
        'city',
        'country',
        'last_visit'
    ];
}
