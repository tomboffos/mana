<?php

declare(strict_types=1);

namespace Domain\History\Repositories;

use Domain\History\Models\History;
use Illuminate\Database\Eloquent\Collection;

interface HistoryRepositoryInterface
{
    public function save(History $order): void;
    public function findByUserId(int $userId);
    public function getLastVisit(int $userId);
}
