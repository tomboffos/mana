<?php

declare(strict_types=1);

namespace Domain\History\Services;

use Domain\History\Contracts\HistoryServiceInterface;
use Domain\History\Data\CreateHistoryDto;
use Domain\History\Models\History;
use Domain\History\Repositories\HistoryRepositoryInterface;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;

class HistoryService implements HistoryServiceInterface
{
    public function __construct(
        private readonly HistoryRepositoryInterface $orderRepository,
    ) {
    }

    public function create(CreateHistoryDto $dto): History
    {
        $order = new History([
            'user_id' => $dto->user_id,
            'gadget_type' => $dto->gadget_type,
            'ip_address' => $dto->ip_address,
            'city' => $dto->city,
            'country' => $dto->country,
            'last_visit' => $dto->last_visit,
        ]);

        $this->orderRepository->save($order);

        return $order;
    }

    public function getHistoryByUser(int $userId)
    {
        return $this->orderRepository->findByUserId($userId);
    }

    public function getLastVisitByUser(int $userId)
    {
        return $this->orderRepository->getLastVisit($userId);
    }
}

