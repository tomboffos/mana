<?php

declare(strict_types=1);

namespace Domain\Host\Contracts;

use Domain\Host\Data\CheckMailDTO;
use Domain\Host\Data\CheckSmtpDTO;

interface HostMailServiceInterface
{
    public function checkMail(CheckMailDTO $checkMailDTO): string;

    public function checkSMTP(CheckSmtpDTO $checkSmtpDTO): bool;
}
