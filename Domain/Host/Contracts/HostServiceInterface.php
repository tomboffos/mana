<?php

declare(strict_types=1);

namespace Domain\Host\Contracts;

use Domain\Host\Models\Host;
use Domain\Instance\Models\Instance;
use Domain\Host\Data\CreateHostDto;
use Domain\Host\Data\UpdateHostDto;
use Illuminate\Database\Eloquent\Collection;

interface HostServiceInterface
{
    public function attachAvailableToInstance(Instance $instance): Host;

    public function detachAllAvailableHosts() : void;

    public function checkIfHostsAvailable() : bool;

    public function updateHostConfig(string $url) : Host;

    public function deleteHostConfig(string $url): Host;

    public function checkWorkingHost(string $url);

    public function getAll(): Collection;

    public function create(CreateHostDto $dto): Host;

    public function update(int $id, UpdateHostDto $dto): ?Host;

    public function delete(int $id): void;
}
