<?php

declare(strict_types=1);

namespace Domain\Host\Data;

class CheckMailDTO
{
    public function __construct(
        public string $url,
        public string $email
    ) {
    }
}
