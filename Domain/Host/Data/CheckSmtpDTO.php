<?php

declare(strict_types=1);

namespace Domain\Host\Data;

class CheckSmtpDTO
{
    public function __construct(
        public string $host,
        public int $port
    ) {
    }
}
