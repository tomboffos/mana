<?php

namespace Domain\Host\Data;

class UpdateHostDto
{
    public function __construct(
        public readonly string $domain,
    ) {
    }


    public function toArray(): array
    {
        return [

            'domain' => $this->domain,
        ];
    }
}
