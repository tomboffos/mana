<?php

namespace Domain\Host\Exceptions;

use Domain\Shared\Exceptions\DomainException;
use Throwable;

class HostDataException extends DomainException
{
    public function __construct(string $message = "", int $code = 409, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getErrorAlias(): string
    {
        return 'data-is-not-match';
    }
}
