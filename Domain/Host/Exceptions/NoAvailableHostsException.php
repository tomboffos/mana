<?php

declare(strict_types=1);

namespace Domain\Host\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class NoAvailableHostsException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'no-available-hosts';
    }
}
