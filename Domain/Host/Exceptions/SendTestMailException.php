<?php

declare(strict_types=1);

namespace Domain\Host\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class SendTestMailException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'cant-send-test-mail';
    }
}
