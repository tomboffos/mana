<?php

declare(strict_types=1);

namespace Domain\Host\Models;

use Domain\Instance\Models\Instance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int|null $instance_id
 * @property int $id
 * @property string $domain
 * @property ?Instance $instance
 */
class Host extends Model
{
    protected $table = 'hosts';
    protected $fillable = [
        'domain',
        'instance_id',
    ];

    public function instance(): BelongsTo
    {
        return $this->belongsTo(Instance::class);
    }
}
