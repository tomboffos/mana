<?php

declare(strict_types=1);

namespace Domain\Host\Repositories;

use Domain\Host\Models\Host;
use Illuminate\Database\Eloquent\Collection;
use Domain\Host\Data\CreateHostDto;
use Domain\Host\Data\UpdateHostDto;

interface HostRepositoryInterface
{
    /** @return Collection<Host> */
    public function getAvailable(): Collection;
    public function save(Host $host): void;

    public function getBusyHosts() : Collection;

    public function findByUrl(string $url) : ?Host;

    public function getAll(): Collection;

    public function create(CreateHostDto $dto): Host;

    public function update(Host $host, UpdateHostDto $dto): Host;

    public function getHostsWithActiveSubscription(): Collection;

    public function delete(Host $host): void;

    public function findById(int $id): ?Host;
}
