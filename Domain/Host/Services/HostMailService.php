<?php

declare(strict_types=1);

namespace Domain\Host\Services;

use Domain\Host\Contracts\HostMailServiceInterface;
use Domain\Host\Data\CheckMailDTO;
use Domain\Host\Data\CheckSmtpDTO;
use Domain\Host\Exceptions\SendTestMailException;
use Domain\Host\Exceptions\SmtpConnectionException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class HostMailService implements HostMailServiceInterface
{
    const MAIL_TEST_ENDPOINT = '/mail/test';
    const CONNECTION_TIMEOUT = 3;

    public function checkMail(CheckMailDTO $checkMailDTO): string
    {
        try {
            $response = Http::timeout(self::CONNECTION_TIMEOUT)
                ->withHeaders([
                    'Accept' => 'application/json',
                    'Content' => 'application/json'
                ])
                ->retry(
                    times: 3,
                    sleepMilliseconds: 100,
                    when: fn($exception) => $exception instanceof ConnectionException
                )->post(
                    url: 'https://' . $checkMailDTO->url . self::MAIL_TEST_ENDPOINT,
                    data: [
                        'secret' => Str::random(10),
                        'email' => $checkMailDTO->email
                    ]
                );
        } catch (\Throwable $throwable) {
            throw new SendTestMailException(message: $throwable->getMessage());
        }

        if ($response->failed() || !isset($response['hash'])) {
            throw new SendTestMailException(message: $response->body());
        }

        return $response['hash'];
    }

    public function checkSMTP(CheckSmtpDTO $checkSmtpDTO): bool
    {
        if (getmxrr(str_replace("https://", "", $checkSmtpDTO->host), $mx_details))
            foreach ($mx_details as $item) {
                try {
                    $f = fsockopen(
                        hostname: $item,
                        port: $checkSmtpDTO->port,
                        timeout: self::CONNECTION_TIMEOUT
                    );

                    if ($f !== false) {

                        $res = fread($f, 1024);
                        if (strlen($res) > 0 && strpos($res, '220') === 0) {
                            return true;

                        } else {
                            if (last(dns_get_record($checkSmtpDTO->host, DNS_MX)) == $item)
                                throw new SmtpConnectionException(
                                    message: 'Cant find 220 status'
                                );
                        }
                    }
                } catch (\Exception$exception) {
                    if (last(dns_get_record($checkSmtpDTO->host, DNS_MX)) == $item)
                        throw new SmtpConnectionException(
                            message: 'Cant find 220 status'
                        );
                }


            }
        throw new SmtpConnectionException(
            message: 'Cant find mx records'
        );


    }
}
