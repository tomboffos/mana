<?php

declare(strict_types=1);

namespace Domain\Host\Services;

use Domain\Host\Contracts\HostMailServiceInterface;
use Domain\Host\Contracts\HostServiceInterface;
use Domain\Host\Data\CheckSmtpDTO;
use Domain\Host\Data\CreateHostDto;
use Domain\Host\Data\UpdateHostDto;
use Domain\Host\Exceptions\NoAvailableHostsException;
use Domain\Host\Models\Host;
use Domain\Host\Repositories\HostRepositoryInterface;
use Domain\Instance\Data\DeleteConfigDto;
use Domain\Instance\Data\UpdateConfigDto;
use Domain\Instance\Gateways\InstanceGatewayInterface;
use Domain\Instance\Models\Instance;
use Domain\Instance\Repositories\InstanceRepositoryInterface;
use Domain\Order\Repositories\OrderRepositoryInterface;
use Domain\Subscription\Exceptions\InstanceNotFoundException;
use Domain\Subscription\Exceptions\SubscriptionNotFoundException;
use Domain\Subscription\Repositories\SubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Service\HostCheckerService;

class HostService implements HostServiceInterface
{
    public function __construct(
        private readonly HostRepositoryInterface         $hostRepository,
        private readonly InstanceRepositoryInterface     $instanceRepository,
        private readonly OrderRepositoryInterface        $orderRepository,
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly InstanceGatewayInterface        $instanceGateway,
        private readonly HostMailServiceInterface        $hostMailService,
    ) {
    }

    public function attachAvailableToInstance(Instance $instance): Host
    {
        $hosts = $this->hostRepository->getAvailable();
        if ($hosts->isEmpty()) {
            throw new NoAvailableHostsException();
        }

        /** @var Host $host */
        $host = $hosts->first();

        $host->instance_id = $instance->id;
        $this->hostRepository->save($host);

        return $host;
    }

    public function detachAllAvailableHosts(): void
    {
        $hosts = $this->hostRepository->getBusyHosts();

        foreach ($hosts as $host) {
            $host->instance_id = null;

            $this->hostRepository->save($host);
        }


        $this->orderRepository->deleteAll();

        $this->subscriptionRepository->deleteAll();

        $this->instanceRepository->deleteAll();
    }

    public function checkIfHostsAvailable(): bool
    {
        return $this->hostRepository->getAvailable()->count() != 0;
    }

    public function updateHostConfig(string $url): Host
    {
        $host = $this->hostRepository->findByUrl($url);


        if (is_null($host))
            throw new NoAvailableHostsException();

        if (is_null($host->instance_id))
            throw new InstanceNotFoundException();

        if (is_null($host->instance->activeSubscription))
            throw new SubscriptionNotFoundException();

        $this->instanceGateway->deleteConfig(new DeleteConfigDto($url));

        $this->instanceGateway->updateConfig(new UpdateConfigDto(
            json_decode($host->instance->activeSubscription->features, true),
            $host->domain,
            $host->instance->activeSubscription->logo_path,
            $host->instance->activeSubscription->name,
            $host->instance->company->user,
            update: false,
            wallets: $host->instance->activeSubscription->wallets_creds != null ? json_decode($host->instance->activeSubscription->wallets_creds, false) : ['']
        ));

        return $host;
    }

    public function deleteHostConfig(string $url): Host
    {
        $host = $this->hostRepository->findByUrl($url);

        if (is_null($host))
            throw new NoAvailableHostsException();

        if (is_null($host->instance_id))
            throw new InstanceNotFoundException();

        if (is_null($host->instance->activeSubscription))
            throw new SubscriptionNotFoundException();

        $this->instanceGateway->deleteConfig(new DeleteConfigDto($url));

        return $host;
    }

    public function checkWorkingHost(string $url)
    {
        $hostChecker = new HostCheckerService($url);

        $host = $this->hostRepository->findByUrl($url);
        return $hostChecker->checkIp()
            && $hostChecker->checkARecords()
            && $hostChecker->checkData($host)
            && $this->hostMailService->checkSMTP(new CheckSmtpDTO(host: $host->domain, port: 25));
    }

    public function create(CreateHostDto $dto): Host
    {
        return $this->hostRepository->create($dto);
    }

    public function update(int $id, UpdateHostDto $dto): ?Host
    {
        $Host = $this->hostRepository->findById($id);

        if (!is_null($Host))
            return $this->hostRepository->update($Host, $dto);

        return null;
    }

    public function delete(int $id): void
    {
        $Host = $this->hostRepository->findById($id);

        if ($Host)
            $this->hostRepository->delete($Host);
    }

    public function getAll(): Collection
    {
        return $this->hostRepository->getAll();
    }
}
