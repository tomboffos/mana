<?php

declare(strict_types=1);

namespace Domain\Instance\Contracts;

use App\Http\Api\Admin\Requests\GetInstanceRequest;
use Domain\Admin\Data\GetInstancesDto;
use Domain\Company\Models\Company;
use Domain\Instance\Data\CreateInstanceDto;
use Domain\Instance\Models\Instance;
use Domain\Subscription\Models\Subscription;
use Domain\User\Models\User;
use Illuminate\Support\Collection;

interface InstanceServiceInterface
{
    public function create(CreateInstanceDto $dto): Instance;

    public function findByCompany(Company $company): ?Instance;

    public function findById(int $id): ?Instance;

    public function save(Instance $instance): Instance;

    public function makeActive(Instance $instance): void;

    public function updateConfig(Instance $instance, Subscription $subscription, ?bool $update): void;

    public function remove(Instance $instance): void;

    public function getInstances(GetInstanceRequest $request);

    public function getDataByFields(string $field) : Collection;

}
