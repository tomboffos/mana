<?php

declare(strict_types=1);

namespace Domain\Instance\Data;

class CreateInstanceDto
{
    public function __construct(
        public readonly int $companyId
    ) {
    }
}
