<?php

declare(strict_types=1);

namespace Domain\Instance\Data;

use Domain\User\Models\User;

class DeleteConfigDto
{
    public function __construct(
        public readonly string $url,
    ) {
    }
}
