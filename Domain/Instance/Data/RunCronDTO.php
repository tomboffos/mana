<?php

namespace Domain\Instance\Data;

class RunCronDTO
{

    public function __construct(
        public readonly int $status,
        public readonly string $url,
        public readonly string $message
    )
    {
    }

}
