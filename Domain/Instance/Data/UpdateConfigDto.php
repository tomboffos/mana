<?php

declare(strict_types=1);

namespace Domain\Instance\Data;

use Domain\User\Models\User;

class UpdateConfigDto
{
    public function __construct(
        public readonly array $config,
        public readonly string $url,
        public readonly ?string $logo,
        public readonly string $name,
        public readonly User $user,
        public readonly ?bool $update,
        public readonly array $wallets
    ) {
    }
}
