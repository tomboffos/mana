<?php

declare(strict_types=1);

namespace Domain\Instance\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class InstanceAlreadyExistsException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'instance-already-exists';
    }
}
