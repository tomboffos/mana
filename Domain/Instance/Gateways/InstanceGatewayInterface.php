<?php

declare(strict_types=1);

namespace Domain\Instance\Gateways;

use Domain\Host\Models\Host;
use Domain\Instance\Data\DeleteConfigDto;
use Domain\Instance\Data\RunCronDTO;
use Domain\Instance\Data\UpdateConfigDto;

interface InstanceGatewayInterface
{
    public function updateConfig(UpdateConfigDto $dto): void;

    public function deleteConfig(DeleteConfigDto $param): void;

    /**
     * @param array $hosts
     * @return RunCronDTO[]
     */
    public function runCrons(array $hosts): array;
}
