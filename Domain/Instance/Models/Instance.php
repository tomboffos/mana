<?php

declare(strict_types=1);

namespace Domain\Instance\Models;

use Domain\Company\Models\Company;
use Domain\Host\Models\Host;
use Domain\Subscription\Models\Subscription;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $company_id
 * @property ?Host $host
 * @property Collection $subscriptions
 * @property boolean $is_active
 * @property ?string $name
 * @property ?string $logo_path
 * @property ?string $admin_password
 * @property-read Company $company
 * @property ?Subscription $activeSubscription
 * @property ?string $external_domain
 */
class Instance extends Model
{
    protected $table = 'instances';
    protected $fillable = [
        'company_id',
        'is_active',
        'name',
        'logo_path',
        'admin_password',
        'external_domain'
    ];

    public function host(): HasOne
    {
        return $this->hasOne(Host::class);
    }

    public function subscriptions(): HasMany
    {
        return $this->hasMany(Subscription::class);
    }

    public function getMainDomain(): ?string
    {
        return $this->external_domain ?? $this->host()->first()?->domain ?? null;
    }

    public function activeSubscription(): HasOne
    {
        return $this->hasOne(Subscription::class)->ofMany([
            'id' => 'max'
        ], function ($query) {
            $query->where('is_active', 1);
        });
    }

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
