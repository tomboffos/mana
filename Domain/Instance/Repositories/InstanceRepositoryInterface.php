<?php

declare(strict_types=1);

namespace Domain\Instance\Repositories;

use App\Http\Api\Admin\Requests\GetInstanceRequest;
use Domain\Instance\Models\Instance;
use Illuminate\Database\Eloquent\Collection;

interface InstanceRepositoryInterface
{
    public function save(Instance $instance): void;

    public function findByCompanyId(int $companyId): Collection;

    public function getInstancesWithActiveSubscriptions(): Collection;

    public function findById(int $id): ?Instance;

    public function getByData(GetInstanceRequest $request): Collection;

    public function deleteAll() : void;

    public function getDataByField(string $field) : \Illuminate\Support\Collection;

}
