<?php

declare(strict_types=1);

namespace Domain\Instance\Services;

use App\Http\Api\Admin\Requests\GetInstanceRequest;
use Domain\Company\Models\Company;
use Domain\Host\Contracts\HostServiceInterface;
use Domain\Instance\Contracts\InstanceServiceInterface;
use Domain\Instance\Data\CreateInstanceDto;
use Domain\Instance\Data\DeleteConfigDto;
use Domain\Instance\Data\UpdateConfigDto;
use Domain\Instance\Exceptions\InstanceAlreadyExistsException;
use Domain\Instance\Gateways\InstanceGatewayInterface;
use Domain\Instance\Models\Instance;
use Domain\Instance\Repositories\InstanceRepositoryInterface;
use Domain\OneSignal\Contracts\OneSignalServiceInterface;
use Domain\OneSignal\Gateways\CreateAppGatewayInterface;
use Domain\ServicesCreateWithHost\Contracts\ServicesCreateWithHostInterface;
use Domain\Subscription\Models\Subscription;
use Domain\Tatum\Gateways\CreateWalletGatewayInterface;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;
use Throwable;

class InstanceService implements InstanceServiceInterface
{
    public function __construct(
        private readonly DatabaseManager             $databaseManager,
        private readonly InstanceRepositoryInterface $instanceRepository,
        private readonly HostServiceInterface        $hostService,
        private readonly CreateAppGatewayInterface $oneSignalGateway,
        private readonly CreateWalletGatewayInterface $tatumWalletsGateway,
        private readonly ServicesCreateWithHostInterface $servicesCreateWithHost,
        public readonly OneSignalServiceInterface $oneSignalService,
        private readonly InstanceGatewayInterface    $instanceGateway,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function create(CreateInstanceDto $dto): Instance
    {
        return $this->databaseManager->transaction(function () use ($dto) {
            $instances = $this->instanceRepository->findByCompanyId($dto->companyId);
            if (!$instances->isEmpty()) {
                throw new InstanceAlreadyExistsException();
            }

            $instance = new Instance([
                'company_id' => $dto->companyId,
                'is_active' => false,
            ]);
            $this->instanceRepository->save($instance);

            $this->hostService->attachAvailableToInstance($instance);

            return $instance;
        });
    }

    public function findByCompany(Company $company): ?Instance
    {
        return $this->instanceRepository->findByCompanyId($company->id)->first();
    }

    public function save(Instance $instance): Instance
    {
        $instance->save();
        return $instance;
    }

    public function findById(int $id): ?Instance
    {
        return $this->instanceRepository->findById($id);
    }

    public function makeActive(Instance $instance): void
    {
        $instance->is_active = true;
        $this->servicesCreateWithHost->createHostNeeds($instance);
        $this->instanceRepository->save($instance);
    }

    public function updateConfig(Instance $instance, Subscription $subscription, ?bool $update): void
    {
        $this->instanceGateway->updateConfig(new UpdateConfigDto(
            json_decode($subscription->features, true),
            $instance->host->domain,
            $subscription->logo_path,
            $subscription->name,
            $instance->company->user,
            update: $update,
            wallets: $subscription->wallets_creds != null ? json_decode($subscription->wallets_creds, true) : ['']
        ));
    }

    public function remove(
        Instance $instance
    ): void {
        $this->instanceGateway->deleteConfig(new DeleteConfigDto(
            $instance->host->domain
        ));
    }

    public function getInstances(GetInstanceRequest $request)
    {
        return $this->instanceRepository->getByData($request);
    }

    public function getDataByFields(string $field): Collection
    {
        return $this->instanceRepository->getDataByField($field);
    }
}
