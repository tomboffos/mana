<?php

declare(strict_types=1);

namespace Domain\OneSignal\Contracts;

use Domain\OneSignal\Data\CreateOneSignalDto;
use Domain\OneSignal\Data\UpdateOneSignalDto;
use Domain\OneSignal\Models\OneSignal;
use Illuminate\Database\Eloquent\Collection;

interface OneSignalServiceInterface
{
    public function getAll(): Collection;


    public function create(CreateOneSignalDto $dto): OneSignal;

    public function update(int $id, UpdateOneSignalDto $dto): ?OneSignal;

    public function delete(int $id): void;
}
