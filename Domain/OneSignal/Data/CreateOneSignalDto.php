<?php

namespace Domain\OneSignal\Data;

class CreateOneSignalDto
{
    public function __construct(
        public readonly string $site_name,
        public readonly string $site_url,
        public readonly string $app_id,
        public readonly string $replica_id,
        public readonly bool   $active,
    )
    {

    }


    public function toArray(): array
    {
        return [
            
            'site_name' => $this->site_name,
            'site_url' => $this->site_url,
            'app_id' => $this->app_id,
            'replica_id' => $this->replica_id,
            'active' => $this->active,
        ];
    }
}
