<?php

declare(strict_types=1);

namespace Domain\OneSignal\Gateways;

use Domain\Shared\Data\CreatedData;
interface CreateAppGatewayInterface
{
    public function createInvoice(array $appInfo): CreatedData;
}
