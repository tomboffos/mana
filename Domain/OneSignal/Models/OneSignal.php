<?php

namespace Domain\OneSignal\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $site_name
 * @property string $site_url
 * @property string $app_id
 * @property string $replica_id
 * @property boolean $active
 */
class OneSignal extends Model
{
    protected $table = 'one_signal_apps';


    protected $fillable = [
        'site_name',
        'site_url',
        'app_id',
        'replica_id',
        'active',
    ];
}
