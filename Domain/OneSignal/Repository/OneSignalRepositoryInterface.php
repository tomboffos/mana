<?php

namespace Domain\OneSignal\Repository;

use Domain\OneSignal\Data\CreateOneSignalDto;
use Domain\OneSignal\Data\UpdateOneSignalDto;
use Domain\OneSignal\Models\OneSignal;
use Illuminate\Database\Eloquent\Collection;

interface OneSignalRepositoryInterface
{
    public function getAll(): Collection;

    public function create(CreateOneSignalDto $dto): OneSignal;

    public function update(OneSignal $OneSignal, UpdateOneSignalDto $dto): OneSignal;

    public function delete(OneSignal $OneSignal): void;

    public function findById(int $id) : ?OneSignal;
    
    public function findByName(int $id): ?OneSignal;
}
