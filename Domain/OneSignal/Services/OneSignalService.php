<?php

declare(strict_types=1);

namespace Domain\OneSignal\Services;

use Domain\OneSignal\Contracts\OneSignalServiceInterface;
use Domain\OneSignal\Data\CreateOneSignalDto;
use Domain\OneSignal\Data\UpdateOneSignalDto;
use Domain\OneSignal\Models\OneSignal;
use Domain\OneSignal\Repository\OneSignalRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OneSignalService implements OneSignalServiceInterface
{
    public function __construct(
        public readonly OneSignalRepositoryInterface $oneSignalRepository
    )
    {

    }

    public function create(CreateOneSignalDto $dto): OneSignal
    {
        return $this->oneSignalRepository->create($dto);
    }

    public function update(int $id, UpdateOneSignalDto $dto): ?OneSignal
    {
        $oneSignal = $this->oneSignalRepository->findById($id);

        if (!is_null($oneSignal))
            return $this->oneSignalRepository->update($oneSignal, $dto);

        return null;
    }

    public function delete(int $id): void
    {
        $oneSignal = $this->oneSignalRepository->findById($id);

        if ($oneSignal)
            $this->oneSignalRepository->delete($oneSignal);
    }

    public function getAll(): Collection
    {
        return $this->oneSignalRepository->getAll();
    }
}
