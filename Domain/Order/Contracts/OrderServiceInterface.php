<?php

declare(strict_types=1);

namespace Domain\Order\Contracts;

use Domain\Order\Data\CreateOrderDto;
use Domain\Order\Data\OrderStatusesEnum;
use Domain\Order\Models\Order;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface OrderServiceInterface
{
    public function create(CreateOrderDto $dto): Order;
    public function findByInvoiceId(string $invoiceId): Order;
    public function makePaid(Order $order): void;

    public function getOrdersByUser(User $user): Collection;

    public function getOrders() : Collection;
}
