<?php

declare(strict_types=1);

namespace Domain\Order\Data;

class CreateOrderDto
{
    public function __construct(
        public readonly int $subscriptionId,
        public readonly float $amount,
    ) {
    }
}
