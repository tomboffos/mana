<?php

namespace Domain\Order\Data;

enum OrderStatusesEnum: string
{
    case CREATED = 'created';
    case PAID = 'paid';
}
