<?php

declare(strict_types=1);

namespace Domain\Order\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class OrderNotFoundException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'order-not-found';
    }
}
