<?php

declare(strict_types=1);

namespace Domain\Order\Gateways;

use Domain\Shared\Data\InvoiceData;

interface PaymentGatewayInterface
{
    public function createInvoice(float $amount): InvoiceData;
}
