<?php

declare(strict_types=1);

namespace Domain\Order\Models;

use Domain\Subscription\Models\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $subscription_id
 * @property float $amount
 * @property string $status
 * @property string $token
 * @property string $external_invoice_id
 * @property Subscription $subscription
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'subscription_id',
        'amount',
        'status',
        'token',
        'external_invoice_id'
    ];

    public function subscription(): BelongsTo
    {
        return $this->belongsTo(Subscription::class, 'subscription_id');
    }
}
