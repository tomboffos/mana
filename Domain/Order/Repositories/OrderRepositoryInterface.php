<?php

declare(strict_types=1);

namespace Domain\Order\Repositories;

use Domain\Order\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    public function save(Order $order): void;

    public function findByInvoiceId(string $invoiceId): ?Order;

    public function findByCompanyId(int $companyId): Collection;

    public function getAll(): Collection;

    public function deleteAll(): void;
}
