<?php

declare(strict_types=1);

namespace Domain\Order\Services;

use Domain\Order\Contracts\OrderServiceInterface;
use Domain\Order\Data\CreateOrderDto;
use Domain\Order\Data\OrderStatusesEnum;
use Domain\Order\Exceptions\OrderNotFoundException;
use Domain\Order\Gateways\PaymentGatewayInterface;
use Domain\Order\Models\Order;
use Domain\Order\Repositories\OrderRepositoryInterface;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;

class OrderService implements OrderServiceInterface
{
    public function __construct(
        private readonly PaymentGatewayInterface $paymentGateway,
        private readonly OrderRepositoryInterface $orderRepository,
    ) {
    }

    public function create(CreateOrderDto $dto): Order
    {
        $invoice = $this->paymentGateway->createInvoice($dto->amount);
        $order = new Order([
            'subscription_id' => $dto->subscriptionId,
            'amount' => $dto->amount,
            'status' => OrderStatusesEnum::CREATED,
            'external_invoice_id' => $invoice->invoiceId,
        ]);

        $this->orderRepository->save($order);

        return $order;
    }

    public function findByInvoiceId(string $invoiceId): Order
    {
        $order = $this->orderRepository->findByInvoiceId($invoiceId);
        if (is_null($order)) {
            throw new OrderNotFoundException();
        }

        return $order;
    }

    public function makePaid(Order $order): void
    {
        $order->status = OrderStatusesEnum::PAID;
        $this->orderRepository->save($order);
    }

    public function getOrdersByUser(User $user): Collection
    {
        return $this->orderRepository->findByCompanyId($user->company_id);
    }

    public function getOrders(): Collection
    {
        return $this->orderRepository->getAll();
    }
}
