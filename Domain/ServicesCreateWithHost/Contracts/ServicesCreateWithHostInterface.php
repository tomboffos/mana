<?php

declare(strict_types=1);

namespace Domain\ServicesCreateWithHost\Contracts;

use Domain\Instance\Models\Instance;

interface ServicesCreateWithHostInterface
{
    public function createHostNeeds(Instance &$instance): void;
}
