<?php

declare(strict_types=1);

namespace Domain\ServicesCreateWithHost\Services;

use Domain\Instance\Models\Instance;
use Domain\OneSignal\Contracts\OneSignalServiceInterface;
use Domain\OneSignal\Data\CreateOneSignalDto;
use Domain\OneSignal\Gateways\CreateAppGatewayInterface;
use Domain\ServicesCreateWithHost\Contracts\ServicesCreateWithHostInterface;
use Domain\Tatum\Gateways\CreateWalletGatewayInterface;


class ServicesCreateWithHost implements ServicesCreateWithHostInterface
{
    public function __construct(
        private readonly CreateAppGatewayInterface $oneSignalGateway,
        private readonly CreateWalletGatewayInterface $tatumWalletsGateway,
        public readonly OneSignalServiceInterface $oneSignalService,
    ) {
    }

    public function createTatumWallets($wallets, Instance $instance): void
    {
        $wallets_creds = $this->tatumWalletsGateway->createInvoice((array) json_decode($wallets));
        $instance->activeSubscription->wallets_creds = $wallets_creds;
        $instance->activeSubscription->save();
    }

    public function createOneSignalApp(Instance $instance): void
    {

        $oneSignal = $this->oneSignalGateway->createInvoice(
            [
                'app_name' => $instance->company->id,
                'domain_url' => $instance->external_domain ?? $instance->host->domain,
                'site_name' => $instance->name
            ]
        );
        $this->oneSignalService->create(
            new CreateOneSignalDto(
                $oneSignal->site_name,
                $oneSignal->domain,
                $oneSignal->id,
                (string)$instance->company->id,
                true
            )
        );
    }

    public function createHostNeeds(Instance &$instance): void
    {
        $wallets = $instance->activeSubscription->wallets;
        if ($wallets !== '["undefined", "{}"]') {
            $this->createTatumWallets($wallets, $instance);
        }
        $this->createOneSignalApp($instance);
    }
}
