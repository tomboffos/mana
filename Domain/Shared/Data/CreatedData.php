<?php

declare(strict_types=1);

namespace Domain\Shared\Data;

class CreatedData
{
    public function __construct(
        public readonly string $id,
        public readonly string $site_name,
        public readonly string $domain,

    ) {
    }
}
