<?php

declare(strict_types=1);

namespace Domain\Shared\Data;

class InvoiceData
{
    public function __construct(
        public readonly string $payUrl,
        public readonly string $invoiceId,
    ) {
    }
}
