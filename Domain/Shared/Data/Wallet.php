<?php

declare(strict_types=1);

namespace Domain\Shared\Data;

class Wallet
{
    public function __construct(
        public readonly string $mnemonic,
        public readonly string $public_key,
        public readonly string $private_key,
        public readonly string $address,

    ) {
    }
}
