<?php

declare(strict_types=1);

namespace Domain\Subscription\Contracts;

use Domain\Subscription\Data\ConfirmSubscriptionDto;
use Domain\Subscription\Data\CreateSubscriptionDto;
use Domain\Subscription\Data\SubscriptionWithOrderDto;
use Domain\Subscription\Data\UpdateSubscriptionDto;
use Domain\Subscription\Models\Subscription;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Domain\Subscription\Data\CheckSubscriptionDto;

interface SubscriptionServiceInterface
{
    public function findForUser(User $user): ?Subscription;

    public function create(CreateSubscriptionDto $dto): SubscriptionWithOrderDto;

    public function confirm(ConfirmSubscriptionDto $dto): void;

    public function update(UpdateSubscriptionDto $dto);

    public function getSubscriptions(): Collection;

    public function checkSubscriptions(): CheckSubscriptionDto;
}
