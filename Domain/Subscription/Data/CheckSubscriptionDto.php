<?php

declare(strict_types=1);

namespace Domain\Subscription\Data;

use Illuminate\Database\Eloquent\Collection;

class CheckSubscriptionDto
{
    public function __construct(
        public readonly ?Collection $deactivated,
        public readonly ?Collection $expiredSoon,
    ) {
    }


    public function toArray() : array
    {
        return [
            'deactivated' => $this->deactivated,
            'expiredSoon' => $this->expiredSoon
        ];
    }
}
