<?php

declare(strict_types=1);

namespace Domain\Subscription\Data;

class ConfirmSubscriptionDto
{
    public function __construct(
        public readonly string $invoiceId,
        public readonly string $status,
    ) {
    }


    public function toArray() : array
    {
        return [
            'invoice_id' => $this->invoiceId,
            'status' => $this->status
        ];
    }
}
