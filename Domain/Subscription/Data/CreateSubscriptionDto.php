<?php

declare(strict_types=1);

namespace Domain\Subscription\Data;

use Domain\User\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;

class CreateSubscriptionDto
{
    public function __construct(
        public readonly User $user,
        public readonly array $features,
        public readonly ?string $name,
        public readonly ?UploadedFile $logo,
        public readonly Carbon $validFrom,
        public readonly Carbon $validUntil,
        public readonly ?string $adminPassword,
        public readonly ?string $externalDomain,
        public readonly ?array $wallets,
    ) {
    }
}
