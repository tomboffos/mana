<?php

declare(strict_types=1);

namespace Domain\Subscription\Data;

use Domain\Order\Models\Order;
use Domain\Subscription\Models\Subscription;

class SubscriptionWithOrderDto
{
    public function __construct(
        public readonly Subscription $subscription,
        public readonly Order $order,
    ) {
    }
}
