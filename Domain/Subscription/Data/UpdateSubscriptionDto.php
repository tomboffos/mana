<?php

namespace Domain\Subscription\Data;

use Domain\User\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;

class UpdateSubscriptionDto
{
    public function __construct(
        public readonly User          $user,
        public readonly string       $name,
        public readonly ?UploadedFile $logo,
        public readonly array        $features,
        public readonly ?Carbon $validFrom,
        public readonly ?Carbon $validUntil,
        public readonly ?int $subscription,
        public readonly ?array $wallets,
        public readonly ?string $externalDomain,
    )
    {

    }
}
