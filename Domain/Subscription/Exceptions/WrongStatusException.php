<?php

declare(strict_types=1);

namespace Domain\Subscription\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class WrongStatusException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'wrong-status';
    }
}
