<?php

namespace Domain\Subscription\Mail;

use App\Mail\BaseMail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SubscriptionConfirmedMail extends BaseMail
{
    use Queueable, SerializesModels;

    protected const SUBJECT = 'Subscription confirmed | Manax Cloud';
    protected const TEMPLATE = 'mail.subscription-confirmed';

    public function __construct(
        string $url,
    )
    {
        $this->data = [
            'url' => $url
        ];

    }
}
