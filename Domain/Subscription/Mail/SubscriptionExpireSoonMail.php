<?php

namespace Domain\Subscription\Mail;

use App\Mail\BaseMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SubscriptionExpireSoonMail extends BaseMail
{
    use Queueable, SerializesModels;

    protected const SUBJECT = 'Subscription expire soon | Manax Cloud';
    protected const TEMPLATE = 'mail.subscription-expire-soon';

    public function __construct(
        string $validUntil,
    )
    {
        $this->data = [
            'validUntil' => $validUntil
        ];

    }
}
