<?php

namespace Domain\Subscription\Mail;

use App\Mail\BaseMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SubscriptionExpiredMail extends BaseMail
{
    use Queueable, SerializesModels;

    protected const SUBJECT = 'Subscription Expired | Manax Cloud';
    protected const TEMPLATE = 'mail.subscription-expired';

    public function __construct(
        string $validUntil,
    )
    {
        $this->data = [
            'validUntil' => $validUntil
        ];

    }
}
