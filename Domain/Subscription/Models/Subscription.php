<?php

declare(strict_types=1);

namespace Domain\Subscription\Models;

use Domain\Instance\Models\Instance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property string $features
 * @property int $id
 * @property string $name
 * @property string $logo_path
 * @property Carbon $valid_until
 * @property int $instance_id
 * @property Instance $instance
 * @property boolean $is_active
 * @property Carbon $valid_from
 * @property string $wallets
 * @property string $wallets_creds
 */
class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = [
        'features',
        'name',
        'logo_path',
        'valid_from',
        'valid_until',
        'instance_id',
        'is_active',
        'wallets',
        'wallets_creds',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function instance(): BelongsTo
    {
        return $this->belongsTo(Instance::class, 'instance_id');
    }
}
