<?php

declare(strict_types=1);

namespace Domain\Subscription\Repositories;

use Domain\Subscription\Models\Subscription;
use Illuminate\Database\Eloquent\Collection;

interface SubscriptionRepositoryInterface
{
    public function findByInstanceId(int $instanceId): ?Subscription;
    public function findById(?int $id): ?Subscription;
    public function save(Subscription $subscription): void;

    public function getAll() : Collection;

    public function getSubscriptionExpireSoon(int $afterDays): ?Collection;

    public function getExpiredSubscriptions(int $daysPassed): ?Collection;

    public function deleteAll() : void;
}
