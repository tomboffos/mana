<?php

declare(strict_types=1);

namespace Domain\Subscription\Services;

use App\Helpers\StorageHelper;
use Domain\DeactivateSubscription\Contracts\DeactivateSubscriptionServiceInterface;
use Domain\Feature\Contracts\FeatureServiceInterface;
use Domain\Instance\Contracts\InstanceServiceInterface;
use Domain\Instance\Models\Instance;
use Domain\Order\Contracts\OrderServiceInterface;
use Domain\Order\Data\CreateOrderDto;
use Domain\Subscription\Contracts\SubscriptionServiceInterface;
use Domain\Subscription\Data\CheckSubscriptionDto;
use Domain\Subscription\Data\ConfirmSubscriptionDto;
use Domain\Subscription\Data\CreateSubscriptionDto;
use Domain\Subscription\Data\SubscriptionWithOrderDto;
use Domain\Subscription\Data\UpdateSubscriptionDto;
use Domain\Subscription\Exceptions\InstanceNotFoundException;
use Domain\Subscription\Exceptions\SubscriptionNotFoundException;
use Domain\Subscription\Exceptions\WrongStatusException;
use Domain\Subscription\Mail\SubscriptionConfirmedMail;
use Domain\Subscription\Mail\SubscriptionExpireSoonMail;
use Domain\Subscription\Models\Subscription;
use Domain\Subscription\Repositories\SubscriptionRepositoryInterface;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Infrastructure\Logging\RequestLoggerHelper;

class SubscriptionService implements SubscriptionServiceInterface
{
    private const SUBSCRIPTION_EXPIRATION_NOTICE_AT_DAYS = 5;
    private const SUBSCRIPTION_EXPIRATION_DEACTIVATE_AT_DAYS = 1;


    public function __construct(
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly InstanceServiceInterface        $instanceService,
        private readonly OrderServiceInterface           $orderService,
        private readonly FeatureServiceInterface         $featureService,
        private readonly DeactivateSubscriptionServiceInterface $deactivateSubscriptionService
    ) {
    }

    public function findForUser(User $user): ?Subscription
    {
        $instance = $this->instanceService->findByCompany($user->company);
        if (is_null($instance)) {
            return null;
        }

        return $this->subscriptionRepository->findByInstanceId($user->id);
    }

    public function create(CreateSubscriptionDto $dto): SubscriptionWithOrderDto
    {
        $user = $dto->user;

        $logoPath = null;
        if (!is_null($dto->logo)) {
            $logoPath = StorageHelper::uploadLogo($dto->logo);
        }

        $instance = $this->instanceService->findByCompany($user->company);
        if (is_null($instance)) {
            throw new InstanceNotFoundException();
        }

        $subscription = new Subscription([
            'features' => json_encode($dto->features),
            'name' => $dto->name ?? $instance->name,
            'logo_path' => $logoPath ?? StorageHelper::DEFAULT_LOGO,
            'valid_from' => $dto->validFrom,
            'valid_until' => $dto->validUntil,
            'instance_id' => $instance->id,
            'is_active' => false,
            'wallets' => json_encode($dto->wallets),
            // 'wallets_creds' => json_encode($dto->wallets_creds)

        ]);
        $this->subscriptionRepository->save($subscription);

        $instance->name = $dto->name ?? $instance->name;
        $instance->logo_path = $dto->logo_path ?? $instance->logo_path;
        $instance->admin_password = $user->password;
        $instance->external_domain = $dto->externalDomain;
        $this->instanceService->save($instance);

        $price = $this->featureService->calculatePrices($dto->features, $dto->validFrom, $dto->validUntil);
        $order = $this->orderService->create(new CreateOrderDto($subscription->id, $price));

        return new SubscriptionWithOrderDto($subscription, $order);
    }

    public function update(UpdateSubscriptionDto $dto)
    {
        $instance = $this->instanceService->findByCompany($dto->user->company);
        if (is_null($instance)) {
            return null;
        }
        $subscription = $this->subscriptionRepository->findByInstanceId($instance->id);
        $logoPath = null;
        if (!is_null($dto->logo)) {
            $logoPath = StorageHelper::uploadLogo($dto->logo);
        }
        if (is_null($subscription)) {
            if (is_null($instance)) {
                throw new InstanceNotFoundException();
            }

            $this->subscriptionRepository->save(new Subscription([
                'features' => json_encode($dto->features),
                'name' => $dto->name ?? $instance->name,
                'logo_path' => $logoPath ?? StorageHelper::DEFAULT_LOGO,
                'valid_from' => $dto->validFrom,
                'valid_until' => $dto->validUntil,
                'instance_id' => $instance->id,
                'is_active' => false,
                'wallets' => json_encode($dto->wallets),
                // 'wallets_creds' => json_encode($dto->wallets_creds)
            ]));

            $this->instanceService->updateConfig($instance, $subscription, null);

            return;
        }

        $subscription->update([
            'logo_path' => $logoPath ?? StorageHelper::DEFAULT_LOGO,
            'name' => $dto->name,
            'features' => json_encode($dto->features),
        ]);
        $instance->external_domain = $dto->externalDomain;
        $instance->name = $dto->name ?? $instance->name;
        $instance->logo_path = $dto->logo != null ? StorageHelper::uploadLogo($dto->logo) : $instance->logo_path;
        $instance->save();

        $instance = $this->instanceService->findById($subscription->instance_id);

        if (is_null($instance)) {
            throw new InstanceNotFoundException();
        }

        $this->instanceService->updateConfig($instance, $subscription, true);
    }

    public function confirm(ConfirmSubscriptionDto $dto): void
    {
        RequestLoggerHelper::logRequest('webhook_payment', data: $dto->toArray());

        if ($dto->status !== 'success') {
            throw new WrongStatusException('Status: ' . $dto->status);
        }

        $order = $this->orderService->findByInvoiceId($dto->invoiceId);
        $this->orderService->makePaid($order);

        $subscription = $this->subscriptionRepository->findById($order->subscription_id);
        if (is_null($subscription)) {
            throw new SubscriptionNotFoundException();
        }

        $subscription->is_active = true;
        $this->subscriptionRepository->save($subscription);

        $instance = $this->instanceService->findById($subscription->instance_id);
        if (is_null($instance)) {
            throw new InstanceNotFoundException();
        }
        $this->instanceService->makeActive($instance);
        $this->instanceService->updateConfig($instance, $subscription, null);

        $this->sendNotification($instance);
    }

    public function sendNotification(
        Instance $instance
    ): void {

        $url = $instance->host()->first()->domain . '/login';
        $user = $instance->company()->first()->user()->first();

        $mail = new SubscriptionConfirmedMail($url);

        Mail::to($user)->send($mail);
    }

    public function getSubscriptions(): Collection
    {
        return $this->subscriptionRepository->getAll();
    }

    public function checkSubscriptions(): CheckSubscriptionDto
    {
        $expiredSubscriptions = $this->subscriptionRepository->getExpiredSubscriptions(self::SUBSCRIPTION_EXPIRATION_DEACTIVATE_AT_DAYS);
        $this->deactivateSubscriptionService->deactivate($expiredSubscriptions);
        $subscriptionExpireSoon = $this->subscriptionRepository->getSubscriptionExpireSoon(self::SUBSCRIPTION_EXPIRATION_NOTICE_AT_DAYS);
        $this->sendNotificationSubscriptionExpiration($subscriptionExpireSoon);

        return new CheckSubscriptionDto($expiredSubscriptions, $subscriptionExpireSoon);
    }

    private function sendNotificationSubscriptionExpiration(?Collection $subscriptions): void
    {
        if ($subscriptions) {
            $subscriptions->each(function ($subscription) {

                $validUntil = $subscription->valid_until;
                $user = $subscription->instance->company->first()->user()->first();

                $mail = new SubscriptionExpireSoonMail($validUntil);

                Mail::to($user)->bcc($mail);
            });
        }
    }
}
