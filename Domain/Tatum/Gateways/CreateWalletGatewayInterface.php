<?php

declare(strict_types=1);

namespace Domain\Tatum\Gateways;

interface CreateWalletGatewayInterface
{
    public function createInvoice(array $walletsInfo): array;
}
