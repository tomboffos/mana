<?php

declare(strict_types=1);

namespace Domain\User\Contracts;

use App\Http\Api\Admin\Requests\GetUsersRequest;
use Domain\Admin\Data\GetUsersDto;
use Domain\User\Data\ChangeResetPasswordDTO;
use Domain\User\Data\CreateUserDto;
use Domain\User\Data\ResetUserPasswordDTO;
use Domain\User\Data\UpdateUserDto;
use Domain\User\Models\User;
use Illuminate\Support\Collection;

interface UserServiceInterface
{
    public function create(CreateUserDto $dto): User;

    public function find(int $userId): User;

    public function findByEmail(string $email): ?User;

    public function save(User $user): void;

    public function update(UpdateUserDto $dto): User;

    public function restorePassword(ResetUserPasswordDTO $resetUserPasswordDTO): void;

    public function changeResetPassword(ChangeResetPasswordDTO $changeUserPasswordDTO): void;

    public function getUsers(GetUsersRequest $request);

    public function getUniqueData(string $field) : Collection;
}
