<?php

declare(strict_types=1);

namespace Domain\User\Data;

class ChangeResetPasswordDTO
{
    public function __construct(
        public readonly string $token,
        public readonly string $password,
    ) {
    }
}
