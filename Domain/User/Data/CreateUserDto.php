<?php

declare(strict_types=1);

namespace Domain\User\Data;

class CreateUserDto
{
    public function __construct(
        public readonly string $name,
        public readonly string $email,
        public readonly string $hashedPassword,
        public readonly int    $companyId,
        public readonly int    $roleId,
    ) {
    }
}
