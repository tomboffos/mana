<?php

namespace Domain\User\Data;

enum PermissionEnum: string
{
    case CAN_CHANGE_PASSWORD_OF_HIS_ACCOUNT = 'can_change_password_of_his_account';
    case TEST_ADMIN_PERMISSION = 'test_admin_permission';
}
