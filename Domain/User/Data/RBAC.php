<?php

namespace Domain\User\Data;


class RBAC
{
    public const ADMIN = 1;
    public const CUSTOMER = 2;

    public const LIST = [
        self::ADMIN => self::ADMIN,
        self::CUSTOMER => self::CUSTOMER,
    ];

    public const PERMISSIONS_TO_ROLES = [
        self::ADMIN => [
            PermissionEnum::CAN_CHANGE_PASSWORD_OF_HIS_ACCOUNT,
            PermissionEnum::TEST_ADMIN_PERMISSION,
        ],
        self::CUSTOMER => [
            PermissionEnum::CAN_CHANGE_PASSWORD_OF_HIS_ACCOUNT,
        ]
    ];

    /**
     * @param PermissionEnum[] $permissionEnums
     * @return string
     */
    public static function anyAbilityForRoute(array $permissionEnums): string
    {
        $imploded = '';
        foreach ($permissionEnums as $enum) {
            $imploded .= $enum->value.',';
        }

        return 'ability:' . rtrim($imploded, ',');
    }

    /**
     * @param PermissionEnum[] $permissionEnums
     * @return string
     */
    public static function allAbilitiesForRoute(array $permissionEnums): string
    {
        $imploded = '';
        foreach ($permissionEnums as $enum) {
            $imploded .= $enum->value.',';
        }
        return 'abilities:' . rtrim($imploded, ',');
    }
}
