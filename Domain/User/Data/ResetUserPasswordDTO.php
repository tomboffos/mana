<?php

declare(strict_types=1);

namespace Domain\User\Data;

class ResetUserPasswordDTO
{
    public function __construct(
        public readonly string $email,
    ) {
    }
}
