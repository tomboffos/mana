<?php

declare(strict_types=1);

namespace Domain\User\Data;

use Domain\User\Models\User;

class UpdateUserDto
{
    public function __construct(
        public readonly User $user,
        public readonly string $name,
        public readonly string $email,
    ) {
    }
}
