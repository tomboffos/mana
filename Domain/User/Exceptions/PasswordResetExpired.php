<?php

declare(strict_types=1);

namespace Domain\User\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class PasswordResetExpired extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'password-reset-expired';
    }
}
