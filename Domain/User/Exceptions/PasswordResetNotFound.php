<?php

declare(strict_types=1);

namespace Domain\User\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class PasswordResetNotFound extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'password-reset-not-found';
    }
}
