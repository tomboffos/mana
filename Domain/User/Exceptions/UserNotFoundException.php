<?php

declare(strict_types=1);

namespace Domain\User\Exceptions;

use Domain\Shared\Exceptions\DomainException;

class UserNotFoundException extends DomainException
{
    public function getErrorAlias(): string
    {
        return 'user-not-found';
    }
}
