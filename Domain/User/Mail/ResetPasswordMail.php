<?php

namespace Domain\User\Mail;

use App\Mail\BaseMail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends BaseMail
{
    use Queueable, SerializesModels;

    protected const SUBJECT = 'Password Reset | Manax Cloud';
    protected const TEMPLATE = 'mail.reset-password';

    public function __construct(
        string $token,
    )
    {
        $this->data = [
            'token' => $token
        ];

    }
}
