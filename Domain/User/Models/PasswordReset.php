<?php

namespace Domain\User\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $token
 * @property string $email
 * @property boolean $status
 * @property \DateTime $created_at
 */
class PasswordReset extends Model
{

    public const TOKEN_TTL = 3600;

    protected $table = 'password_resets';

    protected $casts = [
        'created_at' => 'datetime',
        'status' => 'boolean',
    ];

    public function isExpired(): bool
    {
        return ($this->created_at->getTimestamp() + self::TOKEN_TTL) <= (new \DateTime())->getTimestamp()
            || $this->status
            ;
    }

}
