<?php

declare(strict_types=1);

namespace Domain\User\Repositories;

use App\Http\Api\Admin\Requests\GetUsersRequest;
use Domain\User\Models\User;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function getById(int $userId): ?User;

    public function getByEmail(string $email): ?User;

    public function getUsersByData(GetUsersRequest $request);

    public function getUniqueData(string $field) : Collection;
}
