<?php

declare(strict_types=1);

namespace Domain\User\Services;

use App\Http\Api\Admin\Requests\GetUsersRequest;
use Domain\Admin\Data\GetUsersDto;
use Domain\User\Contracts\UserServiceInterface;
use Domain\User\Data\ChangeResetPasswordDTO;
use Domain\User\Data\CreateUserDto;
use Domain\User\Data\ResetUserPasswordDTO;
use Domain\User\Data\UpdateUserDto;
use Domain\User\Exceptions\PasswordResetExpired;
use Domain\User\Exceptions\PasswordResetNotFound;
use Domain\User\Exceptions\UserNotFoundException;
use Domain\User\Mail\ResetPasswordMail;
use Domain\User\Models\PasswordReset;
use Domain\User\Models\User;
use Domain\User\Repositories\UserRepositoryInterface;
use Illuminate\Hashing\HashManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class UserService implements UserServiceInterface
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly HashManager             $hashManager,
    )
    {
    }

    public function create(CreateUserDto $dto): User
    {
        $user = new User([
            'name' => $dto->name,
            'email' => $dto->email,
            'password' => $dto->hashedPassword,
            'company_id' => $dto->companyId,
            'role_id' => $dto->roleId,
        ]);

        $this->userRepository->save($user);

        return $user;
    }

    public function find(int $userId): User
    {
        $user = $this->userRepository->getById($userId);
        if (is_null($user)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function findByEmail(string $email): ?User
    {
        return $this->userRepository->getByEmail($email);
    }

    public function save(User $user): void
    {
        $this->userRepository->save($user);
    }

    public function update(UpdateUserDto $dto): User
    {
        $dto->user->name = $dto->name;
        $dto->user->email = $dto->email;

        $this->userRepository->save($dto->user);

        return $dto->user;
    }

    public function restorePassword(
        ResetUserPasswordDTO $resetUserPasswordDTO
    ): void
    {
        $passwordReset = new PasswordReset();
        $passwordReset->email = $resetUserPasswordDTO->email;
        $passwordReset->created_at = new \DateTime();
        $passwordReset->token = Str::random(60);
        $passwordReset->status = false;
        $passwordReset->save();

        /** @var User $user */
        $user = User::where('email', $resetUserPasswordDTO->email)->first();

        $mail = new ResetPasswordMail($passwordReset->token);

        Mail::to($user)->send($mail);
    }

    public function changeResetPassword(ChangeResetPasswordDTO $changeUserPasswordDTO): void
    {
        /** @var PasswordReset|null $passwordReset */
        $passwordReset = PasswordReset::query()->where('token', $changeUserPasswordDTO->token)->orderByDesc('created_at')->first();

        if ($passwordReset === null) {
            throw new PasswordResetNotFound();
        }

        if ($passwordReset->isExpired()) {
            throw new PasswordResetExpired();
        }

        $passwordReset->status = true;
        $passwordReset->save();

        /** @var User $user */
        $user = User::where('email', $passwordReset->email)->first();

        $user->password = $this->hashManager->make($changeUserPasswordDTO->password);
        $this->userRepository->save($user);
    }

    public function getUsers(GetUsersRequest $request)
    {
        return $this->userRepository->getUsersByData($request);
    }

    public function getUniqueData(string $field): Collection
    {
        return $this->userRepository->getUniqueData($field);
    }
}
