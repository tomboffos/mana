<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\CryptoCloud;

class CryptoCloudConfig
{
    public function __construct(
        public readonly string $domain,
        public readonly string $apiKey,
        public readonly string $shopId,
        public readonly string $secret
    ) {
    }
}
