<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\CryptoCloud;

use Domain\Order\Gateways\PaymentGatewayInterface;
use Domain\Shared\Data\InvoiceData;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\Client\Response;
use Infrastructure\Gateways\CryptoCloud\Exceptions\CryptoCloudGatewayException;

class CryptoCloudGateway implements PaymentGatewayInterface
{
    private const CREATE_INVOICE_PATH = '/v1/invoice/create';

    public function __construct(
        private readonly CryptoCloudConfig $config,
        private readonly Factory $http,
    ) {
    }

    /**
     * @throws CryptoCloudGatewayException
     */
    public function createInvoice(float $amount): InvoiceData
    {
        $response = $this->post(
            self::CREATE_INVOICE_PATH,
            [
                'shop_id' => $this->config->shopId,
                'amount' => $amount
            ]
        );

        if ($response->status() !== 200) {
            $this->throw($response);
        }

        $data = $response->json();
        if (
            !isset($data['status']) ||
            !isset($data['pay_url']) ||
            !isset($data['invoice_id'])
        ) {
            $this->throw($response);
        }

        if ($data['status'] !== 'success') {
            $this->throw($response);
        }

        return new InvoiceData(
            $data['pay_url'],
            $data['invoice_id'],
        );
    }

    /**
     * @throws CryptoCloudGatewayException
     */
    private function throw(Response $response)
    {
        throw new CryptoCloudGatewayException($response->status(), $response->body());
    }

    private function post(string $url, array $data): Response
    {
        $url = $this->config->domain . $url;

        $headers = $this->prepareHeaders();

        return $this->http->withHeaders($headers)->post($url, $data);
    }

    private function prepareHeaders(): array
    {
        return [
          'Authorization' => 'Token ' . $this->config->apiKey,
        ];
    }
}
