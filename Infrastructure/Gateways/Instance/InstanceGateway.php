<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\Instance;

use App\Helpers\StorageHelper;
use Domain\Host\Models\Host;
use Domain\Instance\Data\DeleteConfigDto;
use Domain\Instance\Data\RunCronDTO;
use Domain\Instance\Data\UpdateConfigDto;
use Domain\Instance\Gateways\InstanceGatewayInterface;
use Domain\Log\Data\RequestResponseLog;
use Domain\OneSignal\Repository\OneSignalRepositoryInterface;
use Domain\Subscription\Services\SubscriptionService;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Client\Pool;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Infrastructure\Logging\RequestLoggerHelper;

class InstanceGateway implements InstanceGatewayInterface
{
    private const UPDATE_CONFIG_URL = '/api/replicaConfig/update';
    private const RUN_CRONS_URL = '/crons';

    public const TYPE_UPDATE = 'update';
    public const TYPE_CREATE = 'create';
    public const TYPE_DELETE = 'delete';

    public function __construct(
        private readonly InstanceGatewayConfig       $config,
        public readonly OneSignalRepositoryInterface $oneSignalRepository
    ) {
    }

    /**
     * @return RunCronDTO[]
     */
    public function runCrons(
        array $hosts
    ): array {

        /** @var Response[] $responses */
        $responses = Http::pool(static fn (Pool $pool) => array_map(
            static fn (string $host) => $pool->get($host . self::RUN_CRONS_URL),
            $hosts
        ));

        $result = [];
        foreach ($responses as $key => $response) {
            if ($response instanceof ConnectException) {
                $result[] = new RunCronDTO(
                    0,
                    $hosts[$key] . self::RUN_CRONS_URL,
                    'connection refused'
                );
            } else {
                $result[] = new RunCronDTO(
                    $response->status(),
                    $hosts[$key] . self::RUN_CRONS_URL,
                    $response->body()
                );
            }
        }

        return $result;
    }

    /**
     * @throws RequestException
     */
    public function updateConfig(UpdateConfigDto $dto): void
    {
        try {
            $url = $dto->url . self::UPDATE_CONFIG_URL;
            $blockChainsWallets = (array) $dto->wallets;
            $tatumWallets = [];
            $blockChains = [];
            foreach ($blockChainsWallets as $blockChain) {
                $blockChain = (array) $blockChain;
                if (array_key_exists("tron", $blockChain)) {
                    $blockChains['TRON'] =  ['minimal_balance' => $blockChain['tron']->minimalBalanceToActivate];
                    $tatumWallets['tron'] = [$blockChain['tron']];
                    unset($tatumWallets['tron'][0]->minimalBalanceToActivate);
                }
                if (array_key_exists("bitcoin", $blockChain)) {
                    $blockChains['BTC'] = ['minimal_balance' => $blockChain['bitcoin']->minimalBalanceToActivate];
                    $tatumWallets['btc'] = [$blockChain['bitcoin']];
                    unset($tatumWallets['btc'][0]->minimalBalanceToActivate);
                }
                if (array_key_exists("ethereum", $blockChain)) {
                    $blockChains['ETH'] = ['minimal_balance' => $blockChain['ethereum']->minimalBalanceToActivate];
                    $tatumWallets['eth'] = [$blockChain['ethereum']];
                    unset($tatumWallets['eth'][0]->minimalBalanceToActivate);
                }
            }
            $oneSignal = $this->oneSignalRepository->findByName($dto->user->company_id);
            $body = [
                'auth_token' => $this->config->authToken,
                'config' => [
                    'addons' => $dto->config,
                    'onesignal' => $oneSignal->app_id ?? '',
                    'blockchains' => $blockChains,
                    'ecosystem_wallets' => $tatumWallets,
                    'smtp' => [
                        'email' => $dto->user->email,
                        'password' => $dto->user->password,
                        'login' => explode('@', $dto->user->email)[0],
                        'port' => 9535,
                        'host' => $dto->url
                    ]
                ],
                'name' => $dto->name,
                'logo' => StorageHelper::getStorageFileLink($dto->logo),
                'adminData' => [
                    'email' => $dto->user->email,
                    'password' => $dto->user->password,
                    'firstName' => $dto->user->name,
                    'username' => explode('@', $dto->user->email)[0],
                    'lastName' => $dto->user->name
                ],
                'type' => $dto->update != null ? self::TYPE_UPDATE : self::TYPE_CREATE,

            ];

            $headers = [
                'Accept' => 'application/json',
            ];

            $response = Http::withHeaders($headers)->post($url, $body);

            RequestLoggerHelper::logRequest('request_output', data: [
                'url' => $url,
                'body' => $body,
                'headers' => $headers,
                'response_status_code' => $response->status(),
                'response' => $response->body(),
            ]);

            $response->throw();
        } catch (\Throwable $throwable) {

            RequestLoggerHelper::logError('slack', data: [
                'message' => $throwable->getMessage(),
            ]);

            return;
        }

    }

    public function deleteConfig(DeleteConfigDto $dto): void
    {
        $response = Http::post($dto->url . self::UPDATE_CONFIG_URL, [
            'auth_token' => $this->config->authToken,
            'type' => self::TYPE_DELETE,
            'config' => []
        ]);
        $response->throw();
    }
}
