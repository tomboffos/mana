<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\Instance;

class InstanceGatewayConfig
{
    public function __construct(
        public readonly string $authToken
    ) {
    }
}
