<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\OneSignal;

class OneSignalConfig
{
    public function __construct(
        public readonly string $domain,
        public readonly string $apiKey,
    ) {
    }
}
