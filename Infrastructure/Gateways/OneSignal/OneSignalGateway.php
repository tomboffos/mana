<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\OneSignal;

use Domain\OneSignal\Gateways\CreateAppGatewayInterface;
use Domain\Shared\Data\CreatedData;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\Client\Response;
use Infrastructure\Gateways\OneSignal\Exceptions\OneSignalGatewayException;

class OneSignalGateway implements CreateAppGatewayInterface
{
    private const CREATE_INVOICE_PATH = '/api/v1/apps/';

    public function __construct(
        private readonly OneSignalConfig $config,
        private readonly Factory $http,
    ) {
    }

    /**
     * @throws OneSignalGatewayException
     */
    public function createInvoice(array $appInfo): CreatedData
    {
        $response = $this->post(
            self::CREATE_INVOICE_PATH,
            [
                'name' => $appInfo['app_name'],
                'chrome_web_origin' => $appInfo['domain_url'],
                'safari_site_origin' => $appInfo['domain_url'],
                'site_name' => $appInfo['site_name']
            ]
        );

        if ($response->status() !== 200) {
            $this->throw($response);
        }

        $data = $response->json();
        if (
            !isset($data['id']) ||
            !isset($data['site_name']) ||
            !isset($data['chrome_web_origin'])
        ) {
            $this->throw($response);
        }

        if ($data['site_name'] !== $appInfo['site_name']) {
            $this->throw($response);
        }

        return new CreatedData(
            $data['id'],
            $data['site_name'] ?? 'jhu',
            $data['chrome_web_origin'] ?? 'uu'
        );
    }

    /**
     * @throws OneSignalGatewayException
     */
    private function throw(Response $response)
    {
        //throw new OneSignalGatewayException($response->status(), $response->body());
    }

    private function post(string $url, array $data): Response
    {
        $url = $this->config->domain . $url;

        $headers = $this->prepareHeaders();

        return $this->http->withHeaders($headers)->post($url, $data);
    }

    private function prepareHeaders(): array
    {
        return [
            'Authorization' => 'Basic ' . $this->config->apiKey,
        ];
    }
}
