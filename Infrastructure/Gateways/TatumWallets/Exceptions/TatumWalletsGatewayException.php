<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\TatumWallets\Exceptions;

use Exception;

class TatumWalletsGatewayException extends Exception
{
    public function __construct(
        private readonly int $status,
        private readonly string $body,
    ) {
        parent::__construct($this->makeMessage(
            $this->status,
            $this->body
        ));
    }

    private function makeMessage(int $code, string $body): string
    {
        return 'StatusCode: ' . $code . ' Body: ' . $body;
    }
}
