<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\TatumWallets;

class TatumWalletsConfig
{
    public function __construct(
        public readonly string $domain,
    ) {
    }
}
