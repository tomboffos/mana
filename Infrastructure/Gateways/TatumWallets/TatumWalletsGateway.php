<?php

declare(strict_types=1);

namespace Infrastructure\Gateways\TatumWallets;

use Domain\Tatum\Gateways\CreateWalletGatewayInterface;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\Client\Response;
use Infrastructure\Gateways\TatumWallets\Exceptions\TatumWalletsGatewayException;

class TatumWalletsGateway implements CreateWalletGatewayInterface
{



    public function __construct(
        private readonly TatumWalletsConfig $config,
        private readonly Factory $http,
    ) {
    }

    private function getWalletPath(string $action, string $wallet): string
    {
        if ($action === 'generate_wallet') {
            return '/v3/' . $wallet . '/wallet';
        } elseif ($action === 'generate_address') {
            return '/v3/' . $wallet . '/address';
        } else if ($action === 'generate_private_key') {
            return '/v3/' . $wallet . '/wallet/priv';
        } else {
            return 'Not Found action';
        }
    }

    /**
     * @throws TatumWalletsGatewayException
     */
    public function createInvoice(array $walletsInfo): array
    {
        $wallets = [];
        $minimalBalanceToActivate = (array) json_decode(array_pop($walletsInfo));
        foreach ($walletsInfo as $key) {
            if ($key !== end($walletsInfo)) {
                $generatedWallet = $this->get(
                    $this->getWalletPath('generate_wallet', $key),
                    end($walletsInfo)
                )->json();
                $generatedAddress = $this->get(
                    $this->getWalletPath('generate_address', $key) . '/' . $generatedWallet['xpub'] . '/0',
                    end($walletsInfo)
                )->json();
                $generatedPrivateKey = $this->post(
                    $this->getWalletPath('generate_private_key', $key),
                    [
                        'index' => 0,
                        'mnemonic' => $generatedWallet['mnemonic'],
                    ],
                    end($walletsInfo)
                )->json();

                array_push($wallets, [
                    $key =>  [
                        'mnemonic' => $generatedWallet['mnemonic'],
                        'public_key' => $generatedWallet['xpub'],
                        'private_key' => $generatedPrivateKey['key'],
                        'address' => $generatedAddress['address'],
                        'minimalBalanceToActivate' => $minimalBalanceToActivate[$key]
                    ]
                ]);
            }
        }

        if (!isset($generatedPrivateKey['key'])) {
            $this->throw($generatedPrivateKey);
        }

        return $wallets;
    }

    /**
     * @throws TatumWalletsGatewayException
     */
    private function throw(Response $response)
    {
        throw new TatumWalletsGatewayException($response->status(), $response->body());
    }

    private function post(string $url, array $data, $apiKey): Response
    {
        $url = $this->config->domain . $url;

        $headers = $this->prepareHeaders($apiKey);

        return $this->http->withHeaders($headers)->post($url, $data);
    }

    private function get(string $url, $apiKey): Response
    {
        $url = $this->config->domain . $url;

        $headers = $this->prepareHeaders($apiKey);

        return $this->http->withHeaders($headers)->get($url);
    }

    private function prepareHeaders($apiKey): array
    {
        return [
            'x-api-key' => $apiKey,
        ];
    }
}
