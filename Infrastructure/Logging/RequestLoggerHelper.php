<?php

namespace Infrastructure\Logging;

use Illuminate\Support\Facades\Log;

class RequestLoggerHelper
{
    static function logRequest(string $channel, array $data): void
    {
        Log::channel($channel)->info(print_r($data, true));
    }


    static function logError(string $channel, array $data): void
    {
        Log::channel($channel)->error(print_r($data, true));
    }


}
