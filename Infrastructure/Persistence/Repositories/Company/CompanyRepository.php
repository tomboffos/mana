<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\Company;

use Domain\Company\Models\Company;
use Domain\Company\Repositories\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function save(Company $company): void
    {
        $company->save();
    }
}
