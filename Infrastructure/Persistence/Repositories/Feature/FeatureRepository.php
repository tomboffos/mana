<?php

namespace Infrastructure\Persistence\Repositories\Feature;

use Domain\Feature\Data\CreateFeatureDto;
use Domain\Feature\Data\UpdateFeatureDto;
use Domain\Feature\Models\Feature;
use Domain\Feature\Repository\FeatureRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FeatureRepository implements FeatureRepositoryInterface
{

    public function getAll(): Collection
    {
        return Feature::get();
    }

    public function create(CreateFeatureDto $dto): Feature
    {
        return Feature::create($dto->toArray());
    }

    public function update(Feature $feature, UpdateFeatureDto $dto): Feature
    {
        $feature->update($dto->toArray());

        return $feature;
    }

    public function delete(Feature $feature): void
    {
        $feature->delete();
    }

    public function findByAlias(string $alias): ?Feature
    {
        return Feature::whereAlias($alias)->first();
    }

    public function findById(int $id): ?Feature
    {
        return Feature::find($id);
    }
}
