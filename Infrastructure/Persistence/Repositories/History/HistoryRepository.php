<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\History;

use Domain\History\Models\History;
use Domain\History\Repositories\HistoryRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class HistoryRepository implements HistoryRepositoryInterface
{
    public function save(History $history): void
    {
        $history->save();
    }

    public function getLastVisit(int $userId)
    {
        return History::where('user_id', $userId)
            ->latest()
            ->get()
            ->first();
    }

    public function findByUserId(int $userid)
    {
        return History::query()
            ->where('user_id', $userid)
            ->get();
    }
}
