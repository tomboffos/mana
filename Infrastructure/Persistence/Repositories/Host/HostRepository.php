<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\Host;

use Doctrine\DBAL\Query;
use Doctrine\DBAL\Query\QueryBuilder;
use Domain\Host\Data\CreateHostDto;
use Domain\Host\Data\UpdateHostDto;
use Domain\Host\Models\Host;
use Domain\Host\Repositories\HostRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;

class HostRepository implements HostRepositoryInterface
{
    public function getAvailable(): Collection
    {
        return Host::query()
            ->whereNull('instance_id')
            ->get();
    }

    public function save(Host $host): void
    {
        $host->save();
    }

    public function getBusyHosts(): Collection
    {
        return Host::query()
            ->whereNotNull('instance_id')
            ->get();
    }

    public function getHostsWithActiveSubscription(): Collection
    {
        return Host::query()
            ->whereHas('instance', function ($query) {
                return $query->whereHas('activeSubscription', function ($query) {
                    return $query->where('is_active', 1);
                });
            })->get();
    }

    public function findByUrl(string $url): ?Host
    {
        return Host::query()->where('domain', $url)->orWhere('domain','https://'.$url)->first();
    }

    public function getAll(): Collection
    {
        return Host::get();
    }

    public function create(CreateHostDto $dto): Host
    {
        return Host::create($dto->toArray());
    }

    public function update(Host $Host, UpdateHostDto $dto): Host
    {
        $Host->update($dto->toArray());

        return $Host;
    }

    public function delete(Host $Host): void
    {
        $Host->delete();
    }

    public function findById(int $id): ?Host
    {
        return Host::find($id);
    }
}
