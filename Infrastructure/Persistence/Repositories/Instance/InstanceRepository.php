<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\Instance;

use App\Http\Api\Admin\Requests\GetInstanceRequest;
use Domain\Instance\Models\Instance;
use Domain\Instance\Repositories\InstanceRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class InstanceRepository implements InstanceRepositoryInterface
{
    public function save(Instance $instance): void
    {
        $instance->save();
    }

    public function getInstancesWithActiveSubscriptions(): Collection
    {
        return Instance::query()
            ->whereHas('activeSubscription', function ($query) {
                return $query->where('is_active', 1);
            })
            ->get();
    }

    public function findByCompanyId(int $companyId): Collection
    {
        return Instance::query()
            ->where('company_id', $companyId)
            ->get();
    }

    public function findById(int $id): ?Instance
    {
        return Instance::query()
            ->where('id', $id)
            ->first();
    }

    public function getByData(GetInstanceRequest $request): Collection
    {
        return Instance::query()->where(function ($query) use ($request) {
            if ($request->has('name')) {
                $query->where('name', $request->name);
            }

            if ($request->has('is_active')) {
                $query->where('is_active', $request->is_active);
            }

            if ($request->has('user_id')) {
                $query->whereHas('company.user', function ($s) use ($request) {
                    $s->where('id', $request->user_id);
                });
            }
        })->get();
    }

    public function deleteAll(): void
    {
        Instance::destroy(Instance::get()->pluck('id'));
    }


    public function getDataByField(string $field): \Illuminate\Support\Collection
    {
        return Instance::query()->distinct($field)->get()->pluck($field);
    }
}
