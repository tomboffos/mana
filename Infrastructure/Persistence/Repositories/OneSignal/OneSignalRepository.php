<?php

namespace Infrastructure\Persistence\Repositories\OneSignal;

use Domain\OneSignal\Data\CreateOneSignalDto;
use Domain\OneSignal\Data\UpdateOneSignalDto;
use Domain\OneSignal\Models\OneSignal;
use Domain\OneSignal\Repository\OneSignalRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OneSignalRepository implements OneSignalRepositoryInterface
{

    public function getAll(): Collection
    {
        return OneSignal::get();
    }

    public function create(CreateOneSignalDto $dto): OneSignal
    {
        return OneSignal::create($dto->toArray());
    }

    public function update(OneSignal $oneSignal, UpdateOneSignalDto $dto): OneSignal
    {
        $oneSignal->update($dto->toArray());

        return $oneSignal;
    }

    public function delete(OneSignal $oneSignal): void
    {
        $oneSignal->delete();
    }

    public function findById(int $id): ?OneSignal
    {
        return OneSignal::find($id);
    }

    public function findByName(int $id): ?OneSignal
    {
        return OneSignal::query()
            ->where('replica_id', (string) $id)->first();
    }
}
