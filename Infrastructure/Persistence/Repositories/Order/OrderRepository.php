<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\Order;

use Domain\Order\Models\Order;
use Domain\Order\Repositories\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class OrderRepository implements OrderRepositoryInterface
{
    public function save(Order $order): void
    {
        $order->save();
    }

    public function findByInvoiceId(string $invoiceId): ?Order
    {
        return Order::query()
            ->where('external_invoice_id', $invoiceId)
            ->first();
    }

    public function findByCompanyId(int $companyId): Collection
    {
        return Order::query()
            ->whereHas('subscription', function (Builder $query) use ($companyId) {
                $query->whereHas('instance', function (Builder $query) use ($companyId) {
                   $query->where('company_id', $companyId);
                });
            })->get();
    }


    public function getAll(): Collection
    {
        return Order::query()->get();
    }

    public function deleteAll() : void
    {
        Order::destroy($this->getAll()->pluck('id'));
    }
}
