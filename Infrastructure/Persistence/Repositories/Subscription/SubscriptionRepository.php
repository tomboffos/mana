<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\Subscription;

use Carbon\Carbon;
use Domain\Subscription\Models\Subscription;
use Domain\Subscription\Repositories\SubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    public function findByInstanceId(int $instanceId): ?Subscription
    {
        return Subscription::query()
            ->where('instance_id', $instanceId)
            ->first();
    }

    public function save(Subscription $subscription): void
    {
        $subscription->save();
    }

    public function findById(?int $id): ?Subscription
    {
        return Subscription::query()
            ->where('id', $id)
            ->first();
    }

    public function getAll(): Collection
    {
        return Subscription::query()->get();
    }

    public function deleteAll(): void
    {
        Subscription::destroy($this->getAll()->pluck('id'));
    }

    public function getSubscriptionExpireSoon(int $afterDays): ?Collection
    {
        return Subscription::query()->where([
            ['valid_until', '<=', Carbon::now()->addDays($afterDays)],
            ['is_active', '=', 1],
        ])->get();
    }

    public function getExpiredSubscriptions(int $daysPassed): ?Collection
    {
        return Subscription::query()->where([
            ['valid_until', '<', Carbon::now()->addDays($daysPassed)],
            ['is_active', '=', 1],
        ])->get();
    }
}
