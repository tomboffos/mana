<?php

declare(strict_types=1);

namespace Infrastructure\Persistence\Repositories\User;

use App\Http\Api\Admin\Requests\GetUsersRequest;
use Domain\User\Models\User;
use Domain\User\Repositories\UserRepositoryInterface;
use Illuminate\Support\Collection;

class UserRepository implements UserRepositoryInterface
{
    public function save(User $user): void
    {
        $user->save();
    }

    public function getById(int $userId): ?User
    {
        return User::query()
            ->where('id', $userId)
            ->first();
    }

    public function getByEmail(string $email): ?User
    {
        return User::query()
            ->where('email', $email)
            ->first();
    }

    public function getUsersByData(GetUsersRequest $request)
    {


        return User::query()->where(function ($query) use ($request) {

            if ($request->has('email')) {
                $query->where('email', $request->email);
            }

            if ($request->has('name')) {
                $query->where('name', $request->name);
            }

            if ($request->has('status')) {
                if ($request->status)
                    $query->whereNotNull('email_verified_at');
                else
                    $query->whereNull('email_verified_at');
            }
        })->get();
    }

    public function getUniqueData(string $field): Collection
    {
        return User::query()->distinct($field)->get()->pluck($field);
    }
}
