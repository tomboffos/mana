<?php

namespace Infrastructure\Security;

use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Laravel\Sanctum\PersonalAccessToken;
use Symfony\Component\HttpFoundation\Exception\SessionNotFoundException;

class CustomSanctumToken extends PersonalAccessToken
{
    protected $table = 'personal_access_tokens';

    public function can($ability): bool
    {
        $tokenable = $this->tokenable()->get();
        if ($tokenable->count() > 1) {
            throw new SessionNotFoundException('Something went wrong on trying to authenticate your token.');
        }

        /** @var User $user */
        $user = $tokenable->first();

        $userAbilities = RBAC::PERMISSIONS_TO_ROLES[$user->role_id];

        foreach ($userAbilities as $userAbility) {
            if ($userAbility->value == $ability) {
                return true;
            }
        }
        return false;
    }
}
