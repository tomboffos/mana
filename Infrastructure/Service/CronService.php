<?php

namespace Infrastructure\Service;

use Domain\Host\Contracts\HostServiceInterface;
use Domain\Host\Repositories\HostRepositoryInterface;
use Domain\Instance\Gateways\InstanceGatewayInterface;
use Domain\Instance\Models\Instance;
use Domain\Instance\Repositories\InstanceRepositoryInterface;

class CronService
{

    public function __construct(
        private readonly InstanceGatewayInterface    $instanceGateway,
        private readonly InstanceRepositoryInterface $instanceRepository,
    )
    {
    }

    public function runCrons(): array
    {
        $instances = $this->instanceRepository->getInstancesWithActiveSubscriptions();
        $hosts = $instances->filter(
            static fn(Instance $instance) => $instance->getMainDomain() !== null
        )
            ->map(
                static fn(Instance $instance) => $instance->getMainDomain()
            )->all();

        $hosts = array_values($hosts);

        return $this->instanceGateway->runCrons($hosts);
    }

}
