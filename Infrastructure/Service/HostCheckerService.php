<?php

namespace Infrastructure\Service;

use App\Mail\TestHostMail;
use Domain\Host\Exceptions\ARecordsNotMatchException;
use Domain\Host\Exceptions\FileValidationException;
use Domain\Host\Exceptions\HostDataException;
use Domain\Host\Exceptions\IpsNotMatchException;
use Domain\Host\Exceptions\SmtpConnectionException;
use Domain\Host\Exceptions\SmtpSendException;
use Domain\Host\Models\Host;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class HostCheckerService
{
    public function __construct(
        private readonly string $host,
    )
    {

    }

    private const AVAILABLE_IPS = [
        '144.76.203.106',
        '78.46.17.109',
        '148.251.14.165'
    ];

    private const AVAILABLE_NS = [

    ];


    public function checkIp(): bool
    {
        return in_array(gethostbyname($this->host), self::AVAILABLE_IPS) ? true : throw new IpsNotMatchException();
    }


    public function checkARecords(): bool
    {
        $record = dns_get_record($this->host, DNS_A);

        if (count($record) > 0 && in_array($record[0]['ip'], self::AVAILABLE_IPS))
            return 1;
        else
            throw new ARecordsNotMatchException();

    }


    public function checkSmtpConnection(): bool
    {

        try {
            Mail::to('no-reply@' . $this->host)->send(new TestHostMail());

            return true;
        } catch (\Exception $exception) {
            throw new SmtpSendException();
        }
    }


    public function checkData(?Host $host): bool
    {

        try {
            $response = Http::get('https://' . $this->host . '/api/replicaConfig/info');
            if (!is_null($host) &&
                !is_null($host->instance->activeSubscription) &&
                !is_null($response->json()) &&
                $host->instance->activeSubscription->name == $response->json()['status']['title'] &&
                'https://' . config('app.host_name') . '/storage/' . $host->instance->activeSubscription->logo_path == $response->json()['status']['logo_path'])
                return true;
            else
                throw new HostDataException();

        } catch (\Throwable $throwable) {
            throw new HostDataException();
        }
    }


}
