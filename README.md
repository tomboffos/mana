
## Installation

``make install``

## Commands

``make up``

``make down``

``make test``

``make migrate``

## Api doc

Документация находится здесь: ``http://localhost/doc`` 

Для генерации документации нужно запустить все тесты командой ``make test``

Запуск конкретного теста пертрет документацию, не забывайте запускать тесты перед каждым коммитом

## Запуск любой команды
В проекте используется Laravel Sail, подробнее можно почитать здесь https://laravel.com/docs/9.x/sail

Artisan команду можно запустить так ``vendor/bin/sail artisan your-command-name``
