<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Domain\Host\Models\Host;
use Exception;
use Illuminate\Console\Command;

class SeedHostsCommand extends Command
{
    protected $signature = 'seed-host';

    /**
     * @throws Exception
     */
    public function handle()
    {
        if (config('app.env') !== 'local') {
            throw new Exception('Wrong app env!');
        }

        $host = new Host([
            'domain' => 'test.domain',
        ]);
        $host->save();
    }
}
