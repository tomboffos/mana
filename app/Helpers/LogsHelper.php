<?php

namespace App\Helpers;

class LogsHelper
{
   public static function isCertbotUrl($url)
   {
      if (strpos($url, env('CERTBOT_URL')) == false) {
         return false;
      }
      return true;
   }
}
