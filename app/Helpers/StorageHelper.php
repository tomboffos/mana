<?php

namespace App\Helpers;

use Domain\Subscription\Services\SubscriptionService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StorageHelper
{

    private const LOGO_PATH = 'logos';
    public const DEFAULT_LOGO = '/assets/images/logoIcon/icon.svg';

    public static function uploadLogo(
        UploadedFile $logo
    ): string
    {
        $logoPath = implode('/', [
            self::LOGO_PATH,
            uniqid() . '.' . $logo->guessExtension() ?? ''
        ]);

        Storage::disk('public')->put($logoPath, $logo->getContent());

        return $logoPath;
    }

    public static function getStorageFileLink(
        string $filename
    ): string
    {
        return $filename !== self::DEFAULT_LOGO ?
            'https://' . config('app.host_name') . '/storage/' . $filename
            : $filename
            ;
    }

}
