<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Feature\Requests\FeatureCreateRequest;
use App\Http\Api\Feature\Requests\FeatureUpdateRequest;
use App\Http\Api\Shared\Resources\Resource;
use App\Http\Controller;
use Domain\Feature\Contracts\FeatureServiceInterface;
use Domain\Feature\Models\Feature;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class AdminFeatureController extends Controller
{
    public function __construct(
        public readonly FeatureServiceInterface $featureService
    )
    {

    }

    public function index(): JsonResource
    {
        $features = $this->featureService->getAll();
        return new Resource($features);
    }

    public function store(FeatureCreateRequest $request): JsonResource
    {
        return new Resource($this->featureService->create($request->toDto()));
    }

    public function update(int $id, FeatureUpdateRequest $request): JsonResource
    {
        return new Resource($this->featureService->update($id, $request->toDto()));
    }

    public function destroy($id): Response
    {

        $this->featureService->delete($id);

        return response([], 200);
    }
}
