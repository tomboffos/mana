<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Admin\Requests\HostCreateRequest;
use App\Http\Api\Admin\Requests\HostUpdateRequest;
use App\Http\Api\Shared\Resources\Resource;
use App\Http\Controller;
use Domain\Host\Contracts\HostServiceInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class AdminHostController extends Controller
{
    public function __construct(
        public readonly HostServiceInterface $hostService
    )
    {

    }

    public function index(): JsonResource
    {
        $Hosts = $this->hostService->getAll();
        return new Resource($Hosts);
    }

    public function store(HostCreateRequest $request): JsonResource
    {
        return new Resource($this->hostService->create($request->toDto()));
    }

    public function update(int $id, HostUpdateRequest $request): JsonResource
    {
        return new Resource($this->hostService->update($id, $request->toDto()));
    }

    public function destroy($id): Response
    {

        $this->hostService->delete($id);

        return response([], 200);
    }
}
