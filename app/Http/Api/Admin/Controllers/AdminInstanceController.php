<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Admin\Requests\GetInstanceRequest;
use App\Http\Api\Shared\Resources\InstanceResource;
use App\Http\Controller;
use Domain\Instance\Contracts\InstanceServiceInterface;

class AdminInstanceController extends Controller
{
    public function __construct(
        private readonly InstanceServiceInterface $instanceService
    )
    {

    }

    public function index(GetInstanceRequest $request)
    {
        $instances = $this->instanceService->getInstances($request);

        return InstanceResource::collection($instances);
    }

    public function getData(string $field)
    {
        return response([
            'data' => $this->instanceService->getDataByFields($field)
        ], 200);
    }
}
