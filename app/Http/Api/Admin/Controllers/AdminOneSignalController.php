<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Admin\Requests\OneSignalCreateRequest;
use App\Http\Api\Admin\Requests\OneSignalUpdateRequest;
use App\Http\Api\Shared\Resources\Resource;
use App\Http\Controller;
use Domain\OneSignal\Contracts\OneSignalServiceInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class AdminOneSignalController extends Controller
{
    public function __construct(
        public readonly OneSignalServiceInterface $oneSignalService
    )
    {

    }

    public function index(): JsonResource
    {
        $oneSignals = $this->oneSignalService->getAll();
        return new Resource($oneSignals);
    }

    public function store(OneSignalCreateRequest $request): JsonResource
    {
        return new Resource($this->oneSignalService->create($request->toDto()));
    }

    public function update(int $id, OneSignalUpdateRequest $request): JsonResource
    {
        return new Resource($this->oneSignalService->update($id, $request->toDto()));
    }

    public function destroy($id): Response
    {

        $this->oneSignalService->delete($id);

        return response([], 200);
    }
}
