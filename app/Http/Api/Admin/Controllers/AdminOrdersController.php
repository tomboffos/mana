<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Shared\Resources\OrderResourceCollection;
use App\Http\Controller;
use Domain\Order\Contracts\OrderServiceInterface;

class AdminOrdersController extends Controller
{
    public function __construct(
        private readonly OrderServiceInterface $orderService
    )
    {

    }

    public function index()
    {
        return new OrderResourceCollection($this->orderService->getOrders());
    }
}
