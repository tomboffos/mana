<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Shared\Resources\SubscriptionResourceCollection;
use App\Http\Controller;
use Domain\Subscription\Contracts\SubscriptionServiceInterface;

class AdminSubscriptionController extends Controller
{
    public function __construct(
        private readonly SubscriptionServiceInterface $service
    )
    {

    }

    public function index()
    {
        return new SubscriptionResourceCollection($this->service->getSubscriptions());
    }
}
