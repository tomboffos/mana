<?php

namespace App\Http\Api\Admin\Controllers;

use App\Http\Api\Admin\Requests\GetUsersRequest;
use App\Http\Api\Shared\Resources\UserResource;
use App\Http\Controller;
use Domain\User\Services\UserService;

class AdminUserController extends Controller
{
    public function __construct(
        private readonly UserService $userService
    )
    {

    }

    public function index(GetUsersRequest $request)
    {
        $users = $this->userService->getUsers($request);

        return UserResource::collection($users);
    }

    public function getData(string $field)
    {
        return response([
            'data' => $this->userService->getUniqueData($field)
        ], 200);
    }
}
