<?php

namespace App\Http\Api\Admin\Requests;

use Domain\Admin\Data\GetInstancesDto;
use Illuminate\Foundation\Http\FormRequest;

class GetInstanceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'is_active' => ['nullable', 'boolean'],
            'user_id' => ['nullable', 'exists:users,id']
        ];
    }


    public function toDto(): GetInstancesDto
    {
        return new GetInstancesDto(
            $this->input('name'),
            $this->input('is_active')
        );
    }

}
