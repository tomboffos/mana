<?php

namespace App\Http\Api\Admin\Requests;

use Domain\Admin\Data\GetUsersDto;
use Illuminate\Foundation\Http\FormRequest;

class GetUsersRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'email' => ['nullable', 'string'],
            'status' => ['nullable', 'boolean'],
        ];
    }

    public function toDto() : GetUsersDto
    {
        return new GetUsersDto(
            $this->input('name'),
            $this->input('status'),
            $this->input('email'),
        );
    }
}
