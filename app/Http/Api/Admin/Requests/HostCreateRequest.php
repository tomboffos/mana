<?php

namespace App\Http\Api\Admin\Requests;

use Domain\Host\Data\CreateHostDto;
use Illuminate\Foundation\Http\FormRequest;

class HostCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'domain' => ['required'],
        ];
    }

    public function toDto(): CreateHostDto
    {
        return new CreateHostDto(
            $this->input('domain'),
        );
    }
}
