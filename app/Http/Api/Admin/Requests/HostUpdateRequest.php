<?php

namespace App\Http\Api\Admin\Requests;

use Domain\Host\Data\UpdateHostDto;
use Illuminate\Foundation\Http\FormRequest;

class HostUpdateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'domain' => ['required'],
        ];
    }

    public function toDto(): UpdateHostDto
    {
        return new UpdateHostDto(
            $this->input('domain'),
        );
    }

}
