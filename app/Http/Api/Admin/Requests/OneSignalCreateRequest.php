<?php

namespace App\Http\Api\Admin\Requests;

use Domain\OneSignal\Data\CreateOneSignalDto;
use Illuminate\Foundation\Http\FormRequest;

class OneSignalCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'site_name' => ['required'],
            'site_url' => ['required'],
            'app_id' => ['required'],
            'replica_id' => ['required'],
            'active' => ['required', 'boolean']
        ];
    }

    public function toDto(): CreateOneSignalDto
    {
        return new CreateOneSignalDto(
            $this->input('site_name'),
            $this->input('site_url'),
            $this->input('app_id'),
            $this->input('replica_id'),
            $this->input('active'),
        );
    }
}
