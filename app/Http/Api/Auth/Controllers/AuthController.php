<?php

declare(strict_types=1);

namespace App\Http\Api\Auth\Controllers;

use App\Http\Api\Auth\Requests\ChangePasswordRequest;
use App\Http\Api\Auth\Requests\LoginUserRequest;
use App\Http\Api\Auth\Requests\RegisterUserRequest;
use App\Http\Api\Auth\Resources\LoggedInUserResource;
use App\Http\Api\Shared\Resources\UserResource;
use App\Http\Controller;
use Domain\Auth\Services\AuthService;
use Throwable;

class AuthController extends Controller
{
    public function __construct(private readonly AuthService $authService)
    {
    }

    public function login(LoginUserRequest $request): LoggedInUserResource
    {
       $user = $this->authService->login($request->toDto());
       return new LoggedInUserResource($user);
    }

    /**
     * @throws Throwable
     */
    public function register(RegisterUserRequest $request): LoggedInUserResource
    {
        $user = $this->authService->register($request->toDto());
        return new LoggedInUserResource($user);
    }

    public function changePassword(ChangePasswordRequest $request): UserResource
    {
        $user = $this->authService->changePassword($request->toDto());
        return new UserResource($user);
    }


    public function loginAdmin(LoginUserRequest $request) : LoggedInUserResource
    {
        $user = $this->authService->loginAdmin($request->toDto());

        return new LoggedInUserResource($user);
    }
}
