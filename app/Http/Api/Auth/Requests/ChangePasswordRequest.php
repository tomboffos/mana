<?php

declare(strict_types=1);

namespace App\Http\Api\Auth\Requests;

use Domain\Auth\Data\ChangePasswordDto;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'old_password' => ['required', 'string'],
            'new_password' => ['required', 'string'],
        ];
    }

    public function toDto(): ChangePasswordDto
    {
        return new ChangePasswordDto(
            $this->user(),
            $this->input('old_password'),
            $this->input('new_password'),
        );
    }
}
