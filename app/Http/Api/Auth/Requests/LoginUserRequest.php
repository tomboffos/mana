<?php

declare(strict_types=1);

namespace App\Http\Api\Auth\Requests;

use Domain\Auth\Data\LoginUserDto;
use Illuminate\Foundation\Http\FormRequest;

class LoginUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => ['email', 'required'],
            'password' => ['string', 'required']
        ];
    }

    public function toDto(): LoginUserDto
    {
        return new LoginUserDto(
            $this->input('email'),
            $this->input('password')
        );
    }
}
