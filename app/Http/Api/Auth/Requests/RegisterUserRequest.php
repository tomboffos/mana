<?php

declare(strict_types=1);

namespace App\Http\Api\Auth\Requests;

use Domain\Auth\Data\RegisterUserDto;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'email' => ['string', 'email'],
            'password' => ['string', 'required'],
        ];
    }

    public function toDto(): RegisterUserDto
    {
        return new RegisterUserDto(
            $this->input('name'),
            $this->input('email'),
            $this->input('password'),
        );
    }
}
