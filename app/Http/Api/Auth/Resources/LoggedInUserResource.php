<?php

declare(strict_types=1);

namespace App\Http\Api\Auth\Resources;

use App\Http\Api\Shared\Resources\Resource;
use App\Http\Api\Shared\Resources\UserResource;
use Domain\Auth\Data\LoggedInUserDto;

/** @property LoggedInUserDto $resource */
class LoggedInUserResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'user' => new UserResource($this->resource->user),
            'token' => $this->resource->newAccessToken->plainTextToken
        ];
    }
}
