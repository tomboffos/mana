<?php

namespace App\Http\Api\Cron;

use App\Http\Api\Shared\Resources\CronResource;
use App\Http\Api\Shared\Resources\EmptyResource;
use App\Http\Api\Shared\Resources\Resource;
use App\Http\Controller;
use Domain\Host\Contracts\HostServiceInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Infrastructure\Service\CronService;

class CronController  extends Controller
{

    public function __construct(
        private readonly CronService $cronService
    ) {}

    public function start(): CronResource
    {
        $result = $this->cronService->runCrons();

        return new CronResource($result);
    }

}
