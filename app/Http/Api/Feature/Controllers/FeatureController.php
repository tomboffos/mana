<?php

declare(strict_types=1);

namespace App\Http\Api\Feature\Controllers;

use App\Http\Api\Shared\Resources\Resource;
use App\Http\Controller;
use Domain\Feature\Contracts\FeatureServiceInterface;

class FeatureController extends Controller
{
    public function __construct(
        private readonly FeatureServiceInterface $featureService,
    ) {
    }

    public function list(): Resource
    {
        $features = $this->featureService->list();

        return new Resource($features);
    }
}
