<?php

namespace App\Http\Api\Feature\Requests;

use Domain\Feature\Data\CreateFeatureDto;
use Illuminate\Foundation\Http\FormRequest;

class FeatureCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'alias' => ['required'],
            'price' => ['required', 'numeric'],
            'active' => ['required', 'boolean']
        ];
    }

    public function toDto(): CreateFeatureDto
    {
        return new CreateFeatureDto(
            $this->input('name'),
            $this->input('alias'),
            $this->input('price'),
            $this->input('active')
        );
    }
}
