<?php

namespace App\Http\Api\Feature\Requests;

use Domain\Feature\Data\CreateFeatureDto;
use Domain\Feature\Data\UpdateFeatureDto;
use Illuminate\Foundation\Http\FormRequest;

class FeatureUpdateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'name' => ['required'],
            'alias' => ['required'],
            'price' => ['required', 'numeric'],
            'active' => ['required', 'boolean']
        ];
    }

    public function toDto(): UpdateFeatureDto
    {
        return new UpdateFeatureDto(
            $this->input('name'),
            $this->input('alias'),
            $this->input('price'),
            $this->input('active')
        );
    }

}
