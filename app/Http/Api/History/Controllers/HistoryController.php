<?php

declare(strict_types=1);

namespace App\Http\Api\History\Controllers;

use App\Http\Api\History\Requests\GetHistoryListRequest;
use App\Http\Api\Shared\Resources\HistoryResource;
use App\Http\Controller;
use Domain\History\Contracts\HistoryServiceInterface;
use Stevebauman\Location\Facades\Location;

class HistoryController extends Controller
{
    public function __construct(private readonly HistoryServiceInterface $historyService)
    {
    }

    public function save(GetHistoryListRequest $request): HistoryResource
    {
        $ip = $request->ip_address;
        $currentUserInfo = Location::get($ip);
        $isMobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);

        $subscription = $this->historyService->create($request->toDto(
            $request->user()->id,
            $currentUserInfo->cityName ?? 'Undefined',
            $currentUserInfo->countryName ?? 'Undefined',
            $ip,
            $isMobile ? 'Mobile' : 'Web'
        ));

        return new HistoryResource($subscription);
    }

    public function list(GetHistoryListRequest $request)
    {
        $orders = $this->historyService->getHistoryByUser($request->user()->id);
        return $orders;
    }

    public function last(GetHistoryListRequest $request)
    {
        $orders = $this->historyService->getLastVisitByUser($request->user()->id);
        return $orders;
    }
}
