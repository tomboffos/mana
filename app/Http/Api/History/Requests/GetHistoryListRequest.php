<?php

declare(strict_types=1);

namespace App\Http\Api\History\Requests;

use Domain\History\Data\CreateHistoryDto;
use Illuminate\Foundation\Http\FormRequest;

class GetHistoryListRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }

    public function toDto($user_id, $cityName, $countryName, $ip, $gadget_type): CreateHistoryDto
    {
        return new CreateHistoryDto($user_id, $gadget_type, $ip, $cityName, $countryName, new \DateTime("now", new \DateTimeZone("UTC")));
    }
}
