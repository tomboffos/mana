<?php

namespace App\Http\Api\Host;

use App\Http\Api\Host\Requests\HostCheckMailRequest;
use App\Http\Api\Host\Requests\HostCheckRequest;
use App\Http\Api\Host\Requests\HostCheckSmtpRequest;
use App\Http\Api\Host\Requests\HostUpdateRequest;
use App\Http\Controller;
use Domain\Host\Contracts\HostMailServiceInterface;
use Domain\Host\Contracts\HostServiceInterface;
use Domain\Host\Data\CheckMailDTO;
use Domain\Host\Data\CheckSmtpDTO;
use Illuminate\Http\Response;

class HostController extends Controller
{
    public function __construct(
        private readonly HostServiceInterface     $hostService,
        private readonly HostMailServiceInterface $hostMailService
    )
    {
    }

    public function clearHosts(): Response
    {
        $this->hostService->detachAllAvailableHosts();

        return response([], 200);
    }

    public function checkHosts(): Response
    {
        return response([
            'available' => $this->hostService->checkIfHostsAvailable(),
        ], 200);
    }


    public function checkHostWork(HostCheckRequest $request)
    {
        return response([
            'status' => $this->hostService->checkWorkingHost($request->url),
        ], 200);
    }

    public function updateHostsConfig(HostUpdateRequest $request): Response
    {
        return response([
            'host' => $this->hostService->updateHostConfig($request->url)
        ]);
    }

    public function deleteHostsConfig(HostUpdateRequest $request): Response
    {
        return response([
            'host' => $this->hostService->deleteHostConfig($request->url)
        ]);
    }

    public function checkHostMail(HostCheckMailRequest $request): Response
    {

        return response([
            'hash' => $this->hostMailService->checkMail(new CheckMailDTO(
                url: $request->url,
                email: $request->email
            )),
            'status' => true,
        ]);
    }

    public function checkHostSmtp(HostCheckSmtpRequest $request): Response
    {
        return response([
            'available' => $this->hostMailService->checkSMTP(new CheckSmtpDTO(
                host: $request->host,
                port: $request->port
            ))
        ]);
    }
}
