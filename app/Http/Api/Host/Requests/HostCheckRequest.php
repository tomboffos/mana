<?php

namespace App\Http\Api\Host\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HostCheckRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'url' => 'required'
        ];
    }
}
