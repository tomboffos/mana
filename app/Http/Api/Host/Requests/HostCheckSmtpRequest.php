<?php

namespace App\Http\Api\Host\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HostCheckSmtpRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'host' => 'required',
            'port' => ['required', 'integer'],

        ];
    }
}
