<?php

declare(strict_types=1);

namespace App\Http\Api\Instance\Controllers;

use App\Http\Api\Instance\Requests\CreateInstanceRequest;
use App\Http\Api\Shared\Resources\EmptyResource;
use App\Http\Api\Shared\Resources\InstanceResource;
use App\Http\Controller;
use Domain\Instance\Contracts\InstanceServiceInterface;

class InstanceController extends Controller
{
    public function __construct(
        private readonly InstanceServiceInterface $instanceService,
    ) {
    }

    public function create(CreateInstanceRequest $request): InstanceResource
    {
        $instance = $this->instanceService->create($request->toDto());
        return new InstanceResource($instance);
    }

    public function remove(int $instanceId): EmptyResource
    {
        $this->instanceService->remove(
            $this->instanceService->findById(
                $instanceId
            )
        );
        return new EmptyResource();
    }
}
