<?php

declare(strict_types=1);

namespace App\Http\Api\Instance\Requests;

use Domain\Instance\Data\CreateInstanceDto;
use Domain\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateInstanceRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }

    public function toDto(): CreateInstanceDto
    {
        /** @var User $user */
        $user = $this->user();
        return new CreateInstanceDto($user->company_id);
    }
}
