<?php

declare(strict_types=1);

namespace App\Http\Api\Order\Controllers;

use App\Http\Api\Order\Requests\GetOrderListRequest;
use App\Http\Api\Shared\Resources\OrderResourceCollection;
use App\Http\Controller;
use Domain\Order\Contracts\OrderServiceInterface;

class OrderController extends Controller
{
    public function __construct(private readonly OrderServiceInterface $orderService)
    {
    }

    public function list(GetOrderListRequest $request): OrderResourceCollection
    {
        $orders = $this->orderService->getOrdersByUser($request->user());
        return new OrderResourceCollection($orders);
    }
}
