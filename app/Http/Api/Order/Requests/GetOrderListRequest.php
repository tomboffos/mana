<?php

declare(strict_types=1);

namespace App\Http\Api\Order\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetOrderListRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }
}
