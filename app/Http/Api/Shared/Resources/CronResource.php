<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use Domain\Instance\Data\RunCronDTO;
use Domain\Instance\Models\Instance;

/** @property RunCronDTO[] $resource */
class CronResource extends Resource
{
    public function toArray($request): array
    {
        return array_map(
            static fn (RunCronDTO $cronDTO) => [
                'status' => $cronDTO->status,
                'url' => $cronDTO->url,
                'message' => $cronDTO->message,
            ],
            $this->resource
        );
    }
}
