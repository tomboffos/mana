<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

class EmptyResource extends Resource
{
    public function __construct()
    {
        parent::__construct(null);
    }
}
