<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use App\Http\Api\Subscriptions\Resources\SubscriptionResource;
use Domain\History\Models\History;

/** @property History $resource */
class HistoryResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'user_id' => $this->user_id,
            'gadget_type' => $this->resource->gadget_type,
            'ip_address' => $this->resource->ip_address,
            'city' => $this->resource->city,
            'country' => $this->resource->country,
            'last_visit' => $this->resource->last_visit,
        ];
    }
}
