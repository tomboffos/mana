<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

class HistoryResourceCollection extends ResourceCollection
{
    public $collects = HistoryResource::class;
}
