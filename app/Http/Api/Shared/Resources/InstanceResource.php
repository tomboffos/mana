<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use Domain\Instance\Models\Instance;

/** @property Instance $resource */
class InstanceResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'company_id' => $this->resource->company_id,
            'subscriptions' => new SubscriptionResourceCollection($this->resource->subscriptions),
            'is_active' => $this->resource->is_active,
            'name' => $this->resource->name,
            'logo_path' => $this->resource->logo_path,
            'admin_password' => $this->resource->ad,
            'user' => $this->resource->company->user,
            'host' => $this->resource->host,
            'external_domain' => $this->resource->external_domain,
        ];
    }
}
