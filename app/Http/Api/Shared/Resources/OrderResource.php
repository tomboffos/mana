<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use App\Http\Api\Subscriptions\Resources\SubscriptionResource;
use Domain\Order\Models\Order;

/** @property Order $resource */
class OrderResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'subscription_id' => $this->resource->subscription_id,
            'subscription' => new SubscriptionResource($this->resource->subscription),
            'amount' => $this->resource->amount,
            'status' => $this->resource->status,
            'external_invoice_id' => $this->resource->external_invoice_id,
        ];
    }
}
