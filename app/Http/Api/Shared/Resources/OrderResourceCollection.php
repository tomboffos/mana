<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

class OrderResourceCollection extends ResourceCollection
{
    public $collects = OrderResource::class;
}
