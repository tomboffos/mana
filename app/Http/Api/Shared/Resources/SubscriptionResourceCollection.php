<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use App\Http\Api\Subscriptions\Resources\SubscriptionResource;

class SubscriptionResourceCollection extends ResourceCollection
{
    public $collects = SubscriptionResource::class;
}
