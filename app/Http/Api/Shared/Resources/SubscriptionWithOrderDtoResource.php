<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;

use App\Http\Api\Subscriptions\Resources\SubscriptionResource;
use Domain\Subscription\Data\SubscriptionWithOrderDto;

/** @property SubscriptionWithOrderDto $resource */
class SubscriptionWithOrderDtoResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'subscription' => new SubscriptionResource($this->resource->subscription),
            'order' => new OrderResource($this->resource->order),
        ];
    }
}
