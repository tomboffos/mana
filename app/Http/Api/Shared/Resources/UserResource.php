<?php

declare(strict_types=1);

namespace App\Http\Api\Shared\Resources;


use Domain\User\Models\User;

/** @property User $resource */
class UserResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'email' => $this->resource->email,
            'email_verified_at' => $this->resource->email_verified_at,
            'status' => $this->resource->hasInstance(),
            'instance' => new InstanceResource($this->resource->company->instance),
            'role_id' => $this->resource->role_id,
            'last_login' => $this->resource->lastLogin
        ];
    }
}
