<?php

declare(strict_types=1);

namespace App\Http\Api\Subscriptions\Controllers;

use App\Http\Api\Shared\Resources\SubscriptionWithOrderDtoResource;
use App\Http\Api\Subscriptions\Requests\SaveSubscriptionsRequest;
use App\Http\Api\Subscriptions\Requests\UpdateSubscriptionsRequest;
use App\Http\Controller;
use Domain\Subscription\Contracts\SubscriptionServiceInterface;
use Domain\Subscription\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class SubscriptionController extends Controller
{
    public function __construct(
        private readonly SubscriptionServiceInterface $subscriptionService,
    )
    {
    }

    public function save(SaveSubscriptionsRequest $request): SubscriptionWithOrderDtoResource
    {
        $subscription = $this->subscriptionService->create($request->toDto());
        return new SubscriptionWithOrderDtoResource($subscription);
    }

    public function update(UpdateSubscriptionsRequest $request): Response
    {
        $this->subscriptionService->update(dto: $request->toDto());

        return response([], 200);
    }

    public function checkSubscription(Request $request): Response
    {
        $subscriptions = $this->subscriptionService->checkSubscriptions();
        Log::channel('slack_subscription')->info(
            env('REPLICA_AUTH_TOKEN') ? 'Production' : 'Localhost',
            array_merge(['Request URL' => $request->url()], ['CRONTAB FOR CHECKING HOSTS SUBSCIPTIONS:' => (array) $subscriptions], ['Method' => $request->method()])
        );
        return response([
            'subscription_info' => $subscriptions,
        ], 200);
    }
}
