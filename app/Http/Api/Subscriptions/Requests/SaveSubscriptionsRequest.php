<?php

declare(strict_types=1);

namespace App\Http\Api\Subscriptions\Requests;

use Domain\Feature\Services\FeatureService;
use Domain\Subscription\Data\CreateSubscriptionDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class SaveSubscriptionsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'features' => ['required', 'array'],
            'features.*' => [
                'required',
                'string',
                'in:' . implode(',', array_keys(FeatureService::FEATURE_LIST))
            ],
            'valid_from' => ['required', 'date'],
            'valid_until' => ['required', 'date'],
            'name' => ['nullable', 'string'],
            'logo' => ['nullable', 'mimes:png,jpg,jpeg', 'max:5120'],
            'admin_password' => ['nullable', 'string'],
            'external_domain' => ['nullable', 'string'],
            'wallets' => ['nullable', 'array'],
        ];
    }

    public function toDto(): CreateSubscriptionDto
    {
        return new CreateSubscriptionDto(
            $this->user(),
            $this->input('features'),
            $this->input('name'),
            $this->file('logo'),
            Carbon::parse($this->input('valid_from')),
            Carbon::parse($this->input('valid_until')),
            $this->input('admin_password'),
            $this->input('external_domain'),
            $this->input('wallets'),
        );
    }
}
