<?php

namespace App\Http\Api\Subscriptions\Requests;

use Domain\Feature\Services\FeatureService;
use Domain\Subscription\Data\UpdateSubscriptionDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class UpdateSubscriptionsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'logo' => ['nullable', 'mimes:png,jpg,jpeg', 'max:5120'],
            'features' => ['required', 'array'],
            'features.*' => [
                'required',
                'string',
                'in:' . implode(',', array_keys(FeatureService::FEATURE_LIST))
            ],
            'subscription' => 'nullable',
            'valid_from' => ['nullable', 'date'],
            'valid_until' => ['nullable', 'date'],
            'wallets' => ['nullable', 'array'],
            'external_domain' => ['nullable', 'string'],
        ];
    }

    public function toDto()
    {
        return new UpdateSubscriptionDto(
            user: $this->user(),
            name: $this->input('name'),
            logo: $this->file('logo'),
            features: $this->input('features'),
            validFrom: Carbon::parse($this->input('valid_from')),
            validUntil: Carbon::parse($this->input('valid_until')),
            subscription: $this->input('subscription'),
            wallets: $this->input('wallets'),
            externalDomain: $this->input('external_domain')
        );
    }
}
