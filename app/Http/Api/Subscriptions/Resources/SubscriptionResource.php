<?php

declare(strict_types=1);

namespace App\Http\Api\Subscriptions\Resources;

use App\Http\Api\Shared\Resources\Resource;
use Domain\Subscription\Models\Subscription;

/** @property Subscription $resource */
class SubscriptionResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'features' => json_decode($this->resource->features, true),
            'name' => $this->resource->name,
            'logo_path' => $this->resource->logo_path,
            'valid_from' => $this->resource->valid_from,
            'valid_until' => $this->resource->valid_until,
            'instance_id' => $this->resource->instance_id,
            'is_active' => $this->resource->is_active,
            'user' => $this->resource->instance->company->user,
            'external_domain' => $this->resource->instance->external_domain,
            'wallets' => $this->resource-> wallets != null ? json_decode($this->resource->wallets, true) : '',
            // 'wallets_creds' => json_decode($this->resource->wallets_creds, true)
        ];
    }
}
