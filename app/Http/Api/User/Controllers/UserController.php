<?php

declare(strict_types=1);

namespace App\Http\Api\User\Controllers;

use App\Http\Api\Shared\Resources\EmptyResource;
use App\Http\Api\Shared\Resources\UserResource;
use App\Http\Api\User\Requests\ResetPasswordRequest;
use App\Http\Api\User\Requests\ChangeResetPasswordRequest;
use App\Http\Api\User\Requests\UpdateUserRequest;
use App\Http\Controller;
use Domain\Auth\Services\AuthService;
use Domain\User\Contracts\UserServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(
        private readonly UserServiceInterface $userService,
    ) {
    }

    public function resetPassword(ResetPasswordRequest $request): EmptyResource
    {
        $this->userService->restorePassword($request->toDto());

        return new EmptyResource();

    }

    public function changeResetPassword(ChangeResetPasswordRequest $request): EmptyResource
    {
        $this->userService->changeResetPassword($request->toDto());

        return new EmptyResource();
    }

    public function current(Request $request): UserResource
    {
        $user = $request->user();
        return new UserResource($user);
    }

    public function update(UpdateUserRequest $request): UserResource
    {
        $user = $this->userService->update($request->toDto());
        return new UserResource($user);
    }
}
