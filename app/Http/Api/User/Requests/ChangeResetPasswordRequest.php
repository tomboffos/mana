<?php

declare(strict_types=1);

namespace App\Http\Api\User\Requests;

use Domain\User\Data\ChangeResetPasswordDTO;
use Domain\User\Data\ResetUserPasswordDTO;
use Domain\User\Data\UpdateUserDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeResetPasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => [
                'required',
                'string',
                'max:255',
                Rule::exists('password_resets', 'token')
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'max:255'
            ],
        ];
    }

    public function toDto(): ChangeResetPasswordDTO
    {
        return new ChangeResetPasswordDTO(
            $this->input('token'),
            $this->input('password'),
        );
    }
}
