<?php

declare(strict_types=1);

namespace App\Http\Api\User\Requests;

use Domain\User\Data\ResetUserPasswordDTO;
use Domain\User\Data\UpdateUserDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ResetPasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::exists('users', 'email')
            ],
        ];
    }

    public function toDto(): ResetUserPasswordDTO
    {
        return new ResetUserPasswordDTO(
            $this->input('email'),
        );
    }
}
