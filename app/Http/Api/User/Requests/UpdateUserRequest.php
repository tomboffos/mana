<?php

declare(strict_types=1);

namespace App\Http\Api\User\Requests;

use Domain\User\Data\UpdateUserDto;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
        ];
    }

    public function toDto(): UpdateUserDto
    {
        return new UpdateUserDto(
            $this->user(),
            $this->input('name'),
            $this->input('email'),
        );
    }
}
