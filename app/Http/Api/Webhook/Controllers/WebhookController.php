<?php

declare(strict_types=1);

namespace App\Http\Api\Webhook\Controllers;

use App\Http\Api\Shared\Resources\EmptyResource;
use App\Http\Api\Webhook\Requests\ConfirmOrderRequest;
use App\Http\Controller;
use Domain\Subscription\Contracts\SubscriptionServiceInterface;

class WebhookController extends Controller
{
    public function __construct(
        private readonly SubscriptionServiceInterface $subscriptionService
    ) {
    }

    public function confirm(ConfirmOrderRequest $request): EmptyResource
    {
        $this->subscriptionService->confirm($request->toDto());

        return new EmptyResource();
    }
}
