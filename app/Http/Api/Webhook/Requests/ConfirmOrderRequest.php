<?php

declare(strict_types=1);

namespace App\Http\Api\Webhook\Requests;

use Domain\Subscription\Data\ConfirmSubscriptionDto;
use Illuminate\Foundation\Http\FormRequest;

class ConfirmOrderRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'status' => ['required', 'string'],
            'invoice_id' => ['required', 'string']
        ];
    }

    public function toDto(): ConfirmSubscriptionDto
    {
        return new ConfirmSubscriptionDto(
            $this->input('invoice_id'),
            $this->input('status'),
        );
    }
}
