<?php

namespace App\Http\Middleware;

use App\Helpers\LogsHelper;
use Closure;
use Illuminate\Support\Facades\Log;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (LogsHelper::isCertbotUrl($request->url()) === false) {
            Log::channel('slack')->info(
                env('REPLICA_AUTH_TOKEN') ? 'Production' : 'Localhost',
                array_merge(['Request URL' => $request->url()], ['Request Body:' => (array) $request->request], ['Method' => $request->method()], ['Client IP' => $request->ip()])
            );
        }
        return $response;
    }
}
