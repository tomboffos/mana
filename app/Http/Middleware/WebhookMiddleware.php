<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Http\Api\Webhook\Requests\ConfirmOrderRequest;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Infrastructure\Gateways\CryptoCloud\CryptoCloudConfig;
use MiladRahimi\Jwt\Exceptions\InvalidKeyException;
use MiladRahimi\Jwt\Exceptions\InvalidSignatureException;
use MiladRahimi\Jwt\Exceptions\InvalidTokenException;
use MiladRahimi\Jwt\Exceptions\JsonDecodingException;
use MiladRahimi\Jwt\Exceptions\SigningException;
use MiladRahimi\Jwt\Exceptions\ValidationException;
use MiladRahimi\Jwt\Parser;
use MiladRahimi\Jwt\Cryptography\Algorithms\Hmac\HS256;
use MiladRahimi\Jwt\Generator;
class WebhookMiddleware
{

    public function __construct(
        private readonly CryptoCloudConfig $cryptoCloudConfig
    ) {}

    public function handle(Request $request, Closure $next)
    {
        $jwt = $request->get('token');

        if ($jwt === null) {
            return response()->json(['message' => 'Invalid JWT'], 403);
        }

        try {
            $signer = new HS256($this->cryptoCloudConfig->secret);

            $parser = new Parser($signer);
            $claims = $parser->parse($jwt);
        } catch (\Throwable $throwable) {
            Log::error('Invalid JWT: ' . $throwable::class . '   ' . $this->cryptoCloudConfig->secret);
            Log::error(json_encode([$request->headers->all(), $request->all()]));
            return response()->json(['message' => 'Invalid JWT' . $throwable::class], 403);
        }

        return $next($request);
    }
}
