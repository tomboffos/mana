<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Mail\MailManager;
use Illuminate\Mail\Message;

class MailService
{
    public function __construct(
        private readonly MailManager $mailManager
    ) {
    }

    public function sendTestMail(string $email): void
    {
        $this->mailManager->raw('Test email', function (Message $message) use ($email){
            $message->to($email);
        });
    }
}
