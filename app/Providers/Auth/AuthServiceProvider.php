<?php

declare(strict_types=1);

namespace App\Providers\Auth;

use Domain\Auth\Contracts\AuthServiceInterface;
use Domain\Auth\Services\AuthService;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public array $singletons = [
        AuthServiceInterface::class => AuthService::class,
    ];
}
