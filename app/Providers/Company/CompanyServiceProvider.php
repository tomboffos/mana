<?php

declare(strict_types=1);

namespace App\Providers\Company;

use Domain\Company\Contracts\CompanyServiceInterface;
use Domain\Company\Repositories\CompanyRepositoryInterface;
use Domain\Company\Services\CompanyService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistence\Repositories\Company\CompanyRepository;

class CompanyServiceProvider extends ServiceProvider
{
    public array $singletons = [
        CompanyServiceInterface::class => CompanyService::class,
        CompanyRepositoryInterface::class => CompanyRepository::class,
    ];
}
