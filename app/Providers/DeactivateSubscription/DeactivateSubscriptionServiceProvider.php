<?php

declare(strict_types=1);

namespace App\Providers\DeactivateSubscription;

use Domain\DeactivateSubscription\Contracts\DeactivateSubscriptionServiceInterface;
use Domain\DeactivateSubscription\Services\DeactivateSubscriptionService;
use Illuminate\Support\ServiceProvider;

class DeactivateSubscriptionServiceProvider extends ServiceProvider
{
    public array $singletons = [
        DeactivateSubscriptionServiceInterface::class => DeactivateSubscriptionService::class,
    ];
}
