<?php

declare(strict_types=1);

namespace App\Providers\Feature;

use Domain\Feature\Contracts\FeatureServiceInterface;
use Domain\Feature\Repository\FeatureRepositoryInterface;
use Domain\Feature\Services\FeatureService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistence\Repositories\Feature\FeatureRepository;

class FeatureServiceProvider extends ServiceProvider
{
    public array $singletons = [
        FeatureServiceInterface::class => FeatureService::class,
        FeatureRepositoryInterface::class => FeatureRepository::class,
    ];
}
