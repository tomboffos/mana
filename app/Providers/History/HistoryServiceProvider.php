<?php

declare(strict_types=1);

namespace App\Providers\History;

use Domain\History\Contracts\HistoryServiceInterface;
use Domain\History\Services\HistoryService;
use Illuminate\Support\ServiceProvider;
use Domain\History\Repositories\HistoryRepositoryInterface;
use Infrastructure\Persistence\Repositories\History\HistoryRepository;

class HistoryServiceProvider extends ServiceProvider
{
    public array $singletons = [
        HistoryServiceInterface::class => HistoryService::class,
        HistoryRepositoryInterface::class => HistoryRepository::class,
    ];
}
