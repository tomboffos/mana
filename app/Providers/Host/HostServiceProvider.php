<?php

declare(strict_types=1);

namespace App\Providers\Host;

use Carbon\Laravel\ServiceProvider;
use Domain\Host\Contracts\HostCheckerInterface;
use Domain\Host\Contracts\HostMailServiceInterface;
use Domain\Host\Contracts\HostServiceInterface;
use Domain\Host\Repositories\HostRepositoryInterface;
use Domain\Host\Services\HostMailService;
use Domain\Host\Services\HostService;
use Infrastructure\Persistence\Repositories\Host\HostRepository;
use Infrastructure\Service\HostCheckerService;

class HostServiceProvider extends ServiceProvider
{
    public array $singletons = [
        HostServiceInterface::class => HostService::class,
        HostMailServiceInterface::class => HostMailService::class,
        HostRepositoryInterface::class => HostRepository::class,
    ];
}
