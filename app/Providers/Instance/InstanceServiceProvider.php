<?php

declare(strict_types=1);

namespace App\Providers\Instance;

use Domain\Instance\Contracts\InstanceServiceInterface;
use Domain\Instance\Gateways\InstanceGatewayInterface;
use Domain\Instance\Repositories\InstanceRepositoryInterface;
use Domain\Instance\Services\InstanceService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Gateways\Instance\InstanceGateway;
use Infrastructure\Gateways\Instance\InstanceGatewayConfig;
use Infrastructure\Persistence\Repositories\Instance\InstanceRepository;

class InstanceServiceProvider extends ServiceProvider
{
    public array $singletons = [
        InstanceServiceInterface::class => InstanceService::class,
        InstanceRepositoryInterface::class => InstanceRepository::class,
        InstanceGatewayInterface::class => InstanceGateway::class,
    ];

    public function register()
    {
        $this->app->when(InstanceGateway::class)
            ->needs(InstanceGatewayConfig::class)
            ->give(fn ($app) => new InstanceGatewayConfig(
                $this->app['config']['services']['instance']['auth_token'],
            ));
    }
}
