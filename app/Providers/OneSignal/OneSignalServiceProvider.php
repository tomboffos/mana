<?php

declare(strict_types=1);

namespace App\Providers\OneSignal;

use Domain\OneSignal\Contracts\OneSignalServiceInterface;
use Domain\OneSignal\Repository\OneSignalRepositoryInterface;
use Domain\OneSignal\Services\OneSignalService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistence\Repositories\OneSignal\OneSignalRepository;

class OneSignalServiceProvider extends ServiceProvider
{
    public array $singletons = [
        OneSignalServiceInterface::class => OneSignalService::class,
        OneSignalRepositoryInterface::class => OneSignalRepository::class,
    ];
}
