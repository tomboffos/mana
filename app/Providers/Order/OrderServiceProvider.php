<?php

declare(strict_types=1);

namespace App\Providers\Order;

use App\Http\Middleware\WebhookMiddleware;
use Carbon\Laravel\ServiceProvider;
use Domain\Order\Contracts\OrderServiceInterface;
use Domain\Order\Gateways\PaymentGatewayInterface;
use Domain\OneSignal\Gateways\CreateAppGatewayInterface;
use Domain\Tatum\Gateways\CreateWalletGatewayInterface;
use Domain\ServicesCreateWithHost\Contracts\ServicesCreateWithHostInterface;
use Domain\ServicesCreateWithHost\Services\ServicesCreateWithHost;
use Domain\Order\Repositories\OrderRepositoryInterface;
use Domain\Order\Services\OrderService;
use Infrastructure\Gateways\CryptoCloud\CryptoCloudConfig;
use Infrastructure\Gateways\CryptoCloud\CryptoCloudGateway;
use Infrastructure\Gateways\OneSignal\OneSignalConfig;
use Infrastructure\Gateways\OneSignal\OneSignalGateway;
use Infrastructure\Gateways\TatumWallets\TatumWalletsGateway;
use Infrastructure\Gateways\TatumWallets\TatumWalletsConfig;
use Infrastructure\Persistence\Repositories\Order\OrderRepository;

class OrderServiceProvider extends ServiceProvider
{
    public array $singletons = [
        PaymentGatewayInterface::class => CryptoCloudGateway::class,
        CreateAppGatewayInterface::class => OneSignalGateway::class,
        CreateWalletGatewayInterface::class => TatumWalletsGateway::class,
        ServicesCreateWithHostInterface::class => ServicesCreateWithHost::class,
        OrderRepositoryInterface::class => OrderRepository::class,
        OrderServiceInterface::class => OrderService::class,
    ];

    public function register()
    {
        $this->app->when(CryptoCloudGateway::class)
            ->needs(CryptoCloudConfig::class)
            ->give(fn ($app) => new CryptoCloudConfig(
                $this->app['config']['services']['crypto_cloud']['domain'],
                $this->app['config']['services']['crypto_cloud']['apiKey'],
                $this->app['config']['services']['crypto_cloud']['shopId'],
                $this->app['config']['services']['crypto_cloud']['secret'],
            ));
        $this->app->when(OneSignalGateway::class)
            ->needs(OneSignalConfig::class)
            ->give(fn ($app) => new OneSignalConfig(
                $this->app['config']['services']['onesignal']['domain'],
                $this->app['config']['services']['onesignal']['apiKey'],
            ));
        $this->app->when(TatumWalletsGateway::class)
            ->needs(TatumWalletsConfig::class)
            ->give(fn ($app) => new TatumWalletsConfig(
                $this->app['config']['services']['tatum']['domain'],
            ));
        $this->app->when(WebhookMiddleware::class)
            ->needs(CryptoCloudConfig::class)
            ->give(fn ($app) => new CryptoCloudConfig(
                $this->app['config']['services']['crypto_cloud']['domain'],
                $this->app['config']['services']['crypto_cloud']['apiKey'],
                $this->app['config']['services']['crypto_cloud']['shopId'],
                $this->app['config']['services']['crypto_cloud']['secret'],
            ));
    }
}
