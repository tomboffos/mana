<?php

declare(strict_types=1);

namespace App\Providers\Subscription;

use Domain\Subscription\Contracts\SubscriptionServiceInterface;
use Domain\Subscription\Repositories\SubscriptionRepositoryInterface;
use Domain\Subscription\Services\SubscriptionService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistence\Repositories\Subscription\SubscriptionRepository;

class SubscriptionServiceProvider extends ServiceProvider
{
    public array $singletons = [
        SubscriptionServiceInterface::class => SubscriptionService::class,
        SubscriptionRepositoryInterface::class => SubscriptionRepository::class,
    ];
}
