<?php

declare(strict_types=1);

namespace App\Providers\User;

use Domain\User\Contracts\UserServiceInterface;
use Domain\User\Repositories\UserRepositoryInterface;
use Domain\User\Services\UserService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistence\Repositories\User\UserRepository;

class UserServiceProvider extends ServiceProvider
{
    public array $singletons = [
        UserServiceInterface::class => UserService::class,
        UserRepositoryInterface::class => UserRepository::class
    ];
}
