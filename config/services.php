<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'crypto_cloud' => [
        'domain' => env('CRYPTO_CLOUD_DOMAIN', 'https://api.cryptocloud.plus'),
        'apiKey' => env('CRYPTO_CLOUD_API_KEY', ''),
        'shopId' => env('CRYPTO_CLOUD_SHOP_ID', ''),
        'secret' => env('CRYPTO_CLOUD_SECRET_KEY', ''),
    ],
    'onesignal' => [
        'domain' => env('ONESIGNAL_DOMAIN', 'https://onesignal.com'),
        'apiKey' => env('ONESIGNAL_AUTH_TOKEN', ''),
    ],
    'tatum' => [
        'domain' => env('TATUM_API_DOMAIN', 'https://api.tatum.io')
    ],
    'instance' => [
        'auth_token' => env('REPLICA_AUTH_TOKEN', ''),
    ],
];
