<?php

declare(strict_types=1);

namespace Database\Factories\Company;

use Domain\Company\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    protected $model = Company::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->numerify,
        ];
    }
}
