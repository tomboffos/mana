<?php

declare(strict_types=1);

namespace Database\Factories\Host;

use Domain\Host\Models\Host;
use Domain\Instance\Models\Instance;
use Illuminate\Database\Eloquent\Factories\Factory;

class HostFactory extends Factory
{
    protected $model = Host::class;

    public function definition(): array
    {
        return [
            'domain' => $this->faker->url,
            'instance_id' => $this->faker->numerify,
        ];
    }

    public function forInstance(Instance $instance): self
    {
        return $this->state(fn (array $attributes) => [
            'instance_id' => $instance->id,
        ]);
    }

    public function withEmptyInstance(): self
    {
        return $this->state(fn (array $attributes) => [
            'instance_id' => null,
        ]);
    }
}
