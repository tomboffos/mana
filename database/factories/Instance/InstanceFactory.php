<?php

declare(strict_types=1);

namespace Database\Factories\Instance;

use Domain\Company\Models\Company;
use Domain\Instance\Models\Instance;
use Illuminate\Database\Eloquent\Factories\Factory;

class InstanceFactory extends Factory
{
    protected $model = Instance::class;

    public function definition(): array
    {
        return [
            'company_id' => $this->faker->numerify,
            'is_active' => $this->faker->boolean,
            'admin_password' => $this->faker->password,
        ];
    }

    public function forCompany(Company $company): self
    {
        return $this->state(fn (array $attributes) => [
            'company_id' => $company->id,
        ]);
    }
}
