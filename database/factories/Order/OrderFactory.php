<?php

declare(strict_types=1);

namespace Database\Factories\Order;

use Domain\Order\Data\OrderStatusesEnum;
use Domain\Order\Models\Order;
use Domain\Subscription\Models\Subscription;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    public $model = Order::class;
    public function definition(): array
    {
        return [
            'subscription_id' => (int) $this->faker->numerify,
            'amount' => (float) $this->faker->numerify,
            'status' => $this->faker->randomElement(OrderStatusesEnum::cases()),
            'external_invoice_id' => $this->faker->numerify
        ];
    }

    public function forSubscription(Subscription $subscription): self
    {
        return $this->state(fn (array $attributes) => [
            'subscription_id' => $subscription->id,
        ]);
    }
}
