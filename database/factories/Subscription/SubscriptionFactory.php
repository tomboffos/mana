<?php

declare(strict_types=1);

namespace Database\Factories\Subscription;

use Domain\Feature\Services\FeatureService;
use Domain\Instance\Models\Instance;
use Domain\Subscription\Models\Subscription;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionFactory extends Factory
{
    protected $model = Subscription::class;

    public function definition(): array
    {
        return [
            'features' => json_encode(array_keys(FeatureService::FEATURE_LIST)),
            'name' => $this->faker->name,
            'logo_path' => $this->faker->filePath(),
            'valid_from' => $this->faker->date,
            'valid_until' => $this->faker->date,
            'instance_id' => $this->faker->numerify,
            'is_active' => $this->faker->boolean,
        ];
    }

    public function forInstance(Instance $instance): self
    {
        return $this->state(fn (array $attributes) => [
            'instance_id' => $instance->id,
        ]);
    }
}
