<?php

declare(strict_types=1);

namespace Database\Factories\User;

use Domain\Company\Models\Company;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->numerify,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'company_id' => $this->faker->numerify,
            'role_id' => $this->faker->randomElement(RBAC::LIST)
        ];
    }

    public function forCompany(Company $company): self
    {
        return $this->state(fn (array $attributes) => [
            'company_id' => $company->id,
        ]);
    }
}
