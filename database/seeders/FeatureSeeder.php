<?php

namespace Database\Seeders;

use Domain\Feature\Models\Feature;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    public const FEATURE_LIST = [
        [
            'name' => 'Bot trader',
            'price' => 25.0,
            'alias' => 'botTrader'
        ],
        [
            'name' => 'Page builder',
            'price' => 25.0,
            'alias' => 'builder'
        ],
        [
            'name' => 'Ecosystem',
            'price' => 25.0,
            'alias' => 'eco'
        ],
        [
            'name' => 'Forex & Investment',
            'price' => 25.0,
            'alias' => 'forex'
        ],
        [
            'name' => 'Token ICO',
            'price' => 25.0,
            'alias' => 'ico'
        ],
        [
            'name' => 'Knowledge base',
            'price' => 25.0,
            'alias' => 'knowledge'
        ],
        [
            'name' => 'LiveChat',
            'price' => 25.0,
            'alias' => 'livechat'
        ],
        [
            'name' => 'MLM',
            'price' => 25.0,
            'alias' => 'mlm'
        ],
        [
            'name' => 'Staking Crypto',
            'price' => 25.0,
            'alias' => 'staking'
        ],
        [
            'name' => 'Wallet Connect',
            'price' => 25.0,
            'alias' => 'walletConnect'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::FEATURE_LIST as $item){
            if (!Feature::whereAlias($item['alias'])->exists())
                Feature::create($item);
        }
    }
}
