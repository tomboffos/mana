<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Password Reset</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        /**
         * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
         */
        @media screen {
            @font-face {
                font-family: 'Source Sans Pro';
                font-style: normal;
                font-weight: 400;
                src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
            }

            @font-face {
                font-family: 'Source Sans Pro';
                font-style: normal;
                font-weight: 700;
                src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
            }
        }

        /**
         * Avoid browser level font resizing.
         * 1. Windows Mobile
         * 2. iOS / OSX
         */
        body,
        table,
        td,
        a {
            -ms-text-size-adjust: 100%; /* 1 */
            -webkit-text-size-adjust: 100%; /* 2 */
        }

        /**
         * Remove extra space added to tables and cells in Outlook.
         */
        table,
        td {
            mso-table-rspace: 0pt;
            mso-table-lspace: 0pt;
        }

        /**
         * Better fluid images in Internet Explorer.
         */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /**
         * Remove blue links for iOS devices.
         */
        a[x-apple-data-detectors] {
            font-family: inherit !important;
            font-size: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            color: inherit !important;
            text-decoration: none !important;
        }

        /**
         * Fix centering issues in Android 4.4.
         */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }

        body {
            width: 100% !important;
            height: 100% !important;
            padding: 0 !important;
            margin: 0 !important;
        }

        /**
         * Collapse table borders to avoid space between cells.
         */
        table {
            border-collapse: collapse !important;
        }

        a {
            color: #1a82e2;
        }

        img {
            height: auto;
            line-height: 100%;
            text-decoration: none;
            border: 0;
            outline: none;
        }
        p {
            color: #e4e7ec;
        }
    </style>

</head>
<body style="background-color: #181e27;">

<!-- start preheader -->
<div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.
</div>
<!-- end preheader -->

<!-- start body -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
        <td align="center" bgcolor="#181e27">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td align="center" valign="top" style="padding: 36px 24px; display: flex; margin-left: 25%">
                        <a href="https://sendgrid.com" target="_blank" style="display: inline-block;">
                            <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDYiIGhlaWdodD0iNDYiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsdGVyPSJ1cmwoI2ZpbHRlcjBfZGRfMjRfNTU5MTkpIj48cGF0aCBkPSJNMzUuNTgzIDI1LjQxNWMuMjI5LjI5OS40NDYuNjA0LjY1My45MTUgMi42MTItLjc1NCA0LjkyMi0yLjMxOCA2Ljc2NC00LjMzNC00LjQ3Ni01LjIxOC0xMi41NjMtNi40Ny0xOC4zMTUtMi43MDQtLjEzNi4wODktLjI3LjE4LS40MDMuMjc1bC0uMDEuMDA3di0uMDA1aC4wMDFjLjE3LS44NzcuNDQxLTEuNzIxLjgwMS0yLjUyIDEuNzUtMy44OSA1LjU5LTYuNzE4IDkuOTI2LTcuMDYxYTExLjk1OCAxMS45NTggMCAwMS0xLjg0MiA1LjQxNiAxNi41MTIgMTYuNTEyIDAgMDEyLjI1Ny4yOThjMS4zMTUtMi4zOCAxLjg0Mi01LjExOSAxLjcxOC03Ljg0Ny02Ljg1NC0uNTI1LTEzLjQ1NyA0LjMwOS0xNC44NjIgMTEuMDM5LS4wMzMuMTU4LS4wNjQuMzE4LS4wOS40OGwtLjAwMS4wMDR2LS4wMDFjLS41LS43NC0uOTA1LTEuNTMtMS4yMTYtMi4zNDktMS41MTItMy45ODctLjc5Ny04LjcwMyAyLjAyNi0xMi4wMWExMS45NTcgMTEuOTU3IDAgMDEyLjUyNyA1LjEzMSAxNi40ODUgMTYuNDg1IDAgMDExLjgwNy0xLjM4NUMyNi41NyA2LjE1MiAyNS4wMDcgMy44NDIgMjIuOTkgMmMtNS4yMTggNC40NzYtNi40NjkgMTIuNTYzLTIuNzAzIDE4LjMxNS4wODguMTM2LjE4LjI3LjI3NC40MDNhLjgwMy44MDMgMCAwMS4wMDkuMDEyIDExLjY0OSAxMS42NDkgMCAwMS0yLjUxNC0uODE2Yy0zLjg2My0xLjc1OS02Ljc0OC01LjU1Ny03LjA3Mi05LjkyNiAxLjkyMy4xNjcgMy43ODMuODAzIDUuNDAyIDEuODM0YTE1Ljk4IDE1Ljk4IDAgMDEuMjk1LTIuMjUzYy0yLjM3LTEuMjc4LTUuMTAyLTEuODYtNy44My0xLjcxNC0uNDU0IDcuMTA0IDQuMzExIDEzLjM0OSAxMC45NzMgMTQuODQ3LjE4Ni4wNDIuMzc0LjA3OS41NjMuMTEzLS43MzkuNDktMS41MjguODktMi4zNSAxLjE5OC0zLjk3NSAxLjQ4OC04LjcwMS44NDMtMTIuMDItMi4wMThhMTIuMDA5IDEyLjAwOSAwIDAxNS4xMTctMi41MjNBMTUuODkgMTUuODkgMCAwMTkuNzUgMTcuNjdjLTIuNTguNzczLTQuOTIzIDIuMjkyLTYuNzUgNC4zMjUgNC43MDMgNS4zNDQgMTIuNDg5IDYuMzkgMTguMjU4IDIuNzQuMTU3LS4xLjMxMS0uMjAzLjQ2NS0uMzFhMTEuNjQ0IDExLjY0NCAwIDAxLS44MTUgMi41MDhjLTEuNzU4IDMuODYzLTUuNTU3IDYuNzQ4LTkuOTI1IDcuMDcyYTEyLjAwNyAxMi4wMDcgMCAwMTEuODMzLTUuNDAyIDE1LjkwNyAxNS45MDcgMCAwMS0yLjI1My0uMjk1Yy0xLjI3NyAyLjM3LTEuODYgNS4xMDItMS43MTQgNy44MyA3LjEwNC40NTQgMTMuMzUtNC4zMTIgMTQuODQ3LTEwLjk3My4wNDItLjE4My4wNzgtLjM2OC4xMTItLjU1NC40OS43NC44OTEgMS41MyAxLjIgMi4zNTIgMS40ODcgMy45NzUuODQyIDguNzAxLTIuMDE4IDEyLjAyYTEyLjAwOCAxMi4wMDggMCAwMS0yLjUyNC01LjExN2MtLjU3LjUwNi0xLjE3MS45NjgtMS44MDEgMS4zODUuNzczIDIuNTggMi4yOTIgNC45MjIgNC4zMjUgNi43NDkgNS4zNDQtNC43MDMgNi4zOS0xMi40ODkgMi43MzktMTguMjU4LS4wOTktLjE1Ni0uMjAyLS4zMS0uMzA4LS40NjMuODc2LjE3IDEuNzIuNDQxIDIuNTIuOHYuMDAxYzMuODg5IDEuNzUgNi43MTggNS41OSA3LjA2IDkuOTI1YTExLjk1NiAxMS45NTYgMCAwMS01LjQxNi0xLjg0MiAxNi44MSAxNi44MSAwIDAxLS4yOTggMi4yNTdjMi4zOCAxLjMxNSA1LjExOSAxLjg0MyA3Ljg0NyAxLjcxOS41MjUtNi44NTQtNC4zMDktMTMuNDU3LTExLjAzOS0xNC44NjJhMTEuNjg5IDExLjY4OSAwIDAwLS40NzQtLjA5bC4wMDItLjAwMmMuNzQtLjUgMS41My0uOTA1IDIuMzQ5LTEuMjE2IDMuOTg3LTEuNTEyIDguNzAzLS43OTcgMTIuMDEgMi4wMjZhMTEuOTYgMTEuOTYgMCAwMS01LjEzMSAyLjUyN2MuMjUzLjI4OC40OTcuNTg1LjczMi44OTN6IiBmaWxsPSJ1cmwoI3BhaW50MF9saW5lYXJfMjRfNTU5MTkpIi8+PC9nPjxkZWZzPjxsaW5lYXJHcmFkaWVudCBpZD0icGFpbnQwX2xpbmVhcl8yNF81NTkxOSIgeDE9IjMiIHkxPSIyMiIgeDI9IjQzIiB5Mj0iMjIiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj48c3RvcCBzdG9wLWNvbG9yPSIjNzk1MUU3Ii8+PHN0b3Agb2Zmc2V0PSIxIiBzdG9wLWNvbG9yPSIjODE2NUNDIi8+PC9saW5lYXJHcmFkaWVudD48ZmlsdGVyIGlkPSJmaWx0ZXIwX2RkXzI0XzU1OTE5IiB4PSIwIiB5PSIwIiB3aWR0aD0iNDYiIGhlaWdodD0iNDYiIGZpbHRlclVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzPSJzUkdCIj48ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PSIwIiByZXN1bHQ9IkJhY2tncm91bmRJbWFnZUZpeCIvPjxmZUNvbG9yTWF0cml4IGluPSJTb3VyY2VBbHBoYSIgdmFsdWVzPSIwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCIgcmVzdWx0PSJoYXJkQWxwaGEiLz48ZmVPZmZzZXQgZHk9IjEiLz48ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPSIxIi8+PGZlQ29sb3JNYXRyaXggdmFsdWVzPSIwIDAgMCAwIDAuMDYyNzQ1MSAwIDAgMCAwIDAuMDk0MTE3NiAwIDAgMCAwIDAuMTU2ODYzIDAgMCAwIDAuMDYgMCIvPjxmZUJsZW5kIGluMj0iQmFja2dyb3VuZEltYWdlRml4IiByZXN1bHQ9ImVmZmVjdDFfZHJvcFNoYWRvd18yNF81NTkxOSIvPjxmZUNvbG9yTWF0cml4IGluPSJTb3VyY2VBbHBoYSIgdmFsdWVzPSIwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCIgcmVzdWx0PSJoYXJkQWxwaGEiLz48ZmVPZmZzZXQgZHk9IjEiLz48ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPSIxLjUiLz48ZmVDb2xvck1hdHJpeCB2YWx1ZXM9IjAgMCAwIDAgMC4wNjI3NDUxIDAgMCAwIDAgMC4wOTQxMTc2IDAgMCAwIDAgMC4xNTY4NjMgMCAwIDAgMC4xIDAiLz48ZmVCbGVuZCBpbjI9ImVmZmVjdDFfZHJvcFNoYWRvd18yNF81NTkxOSIgcmVzdWx0PSJlZmZlY3QyX2Ryb3BTaGFkb3dfMjRfNTU5MTkiLz48ZmVCbGVuZCBpbj0iU291cmNlR3JhcGhpYyIgaW4yPSJlZmZlY3QyX2Ryb3BTaGFkb3dfMjRfNTU5MTkiIHJlc3VsdD0ic2hhcGUiLz48L2ZpbHRlcj48L2RlZnM+PC9zdmc+" alt="Logo" border="0" width="48" style="display: block; width: 48px; max-width: 48px; min-width: 48px;">
                        </a>
                        <h2 style="color: #fff; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; margin-left: 2vh; line-height: 0; font-size: 27px;">Manax Cloud</h2>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
        <td align="center" bgcolor="#181e27">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-radius: 20px 20px 0px 0px; background: #1f2733;">
                        <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px; color: #fff;">Reset Your Password</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
        <td align="center" bgcolor="#181e27">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

                <!-- start copy -->
                <tr>
                    <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background: #1f2733;">
                        <p style="margin: 0;">Tap the button below to reset your customer account password. If you didn't request a new password, you can safely delete this email.</p>
                    </td>
                </tr>
                <!-- end copy -->

                <!-- start button -->
                <tr>
                    <td align="left" bgcolor="#1f2733">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" bgcolor="#1f2733" style="padding: 12px;">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center" bgcolor="#7f56d9" style="border-radius: 8px;">
                                                <a href="https://manax.cloud/reset-password?token={{ $token }}" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Reset password</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- end button -->

                <!-- start copy -->
                </tr>
                <!-- end copy -->

                <!-- start copy -->
                <tr>
                    <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-radius: 0px 0px 20px 20px; background: #1f2733;">
                        <p style="margin: 0;">Cheers,<br> Manax Cloud</p>
                    </td>
                </tr>
                <!-- end copy -->

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- end copy block -->

    <!-- start footer -->
    <tr>
        <td align="center" bgcolor="#181e27" style="padding: 24px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

                <!-- start permission -->
                <tr>
                    <td align="center" bgcolor="#181e27" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
                        <p style="margin: 0;">You received this email because we received a request for your account. If you didn't request you can safely delete this email.</p>
                    </td>
                </tr>
                <!-- end permission -->

                <!-- start unsubscribe -->
                <tr>
                    <!-- end unsubscribe -->

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- end footer -->

</table>
<!-- end body -->

</body>
</html>
