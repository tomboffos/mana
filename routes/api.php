<?php

use App\Http\Api\Admin\Controllers\AdminFeatureController;
use App\Http\Api\Admin\Controllers\AdminInstanceController;
use App\Http\Api\Admin\Controllers\AdminOneSignalController;
use App\Http\Api\Admin\Controllers\AdminHostController;
use App\Http\Api\Admin\Controllers\AdminOrdersController;
use App\Http\Api\Admin\Controllers\AdminSubscriptionController;
use App\Http\Api\Admin\Controllers\AdminUserController;
use App\Http\Api\Auth\Controllers\AuthController;
use App\Http\Api\Cron\CronController;
use App\Http\Api\Feature\Controllers\FeatureController;
use App\Http\Api\Host\HostController;
use App\Http\Api\Instance\Controllers\InstanceController;
use App\Http\Api\Order\Controllers\OrderController;
use App\Http\Api\History\Controllers\HistoryController;
use App\Http\Api\Shared\Resources\EmptyResource;
use App\Http\Api\Subscriptions\Controllers\SubscriptionController;
use App\Http\Api\User\Controllers\UserController;
use App\Http\Api\Webhook\Controllers\WebhookController;
use App\Http\Middleware\AllowOnlyJsonMiddleware;
use App\Http\Middleware\WebhookMiddleware;
use Domain\User\Data\PermissionEnum;
use Domain\User\Data\RBAC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/subscription-check', [SubscriptionController::class, 'checkSubscription'])
    ->name('subscription.check');

Route::group(['prefix' => 'auth'], function () {
    Route::get('/', fn () => new EmptyResource())
        ->middleware('auth:sanctum')
        ->name('auth.test');

    Route::post('changePassword', [AuthController::class, 'changePassword'])
        ->middleware('auth:sanctum', RBAC::anyAbilityForRoute([PermissionEnum::CAN_CHANGE_PASSWORD_OF_HIS_ACCOUNT]))
        ->name('auth.changePassword');

    Route::post('register', [AuthController::class, 'register'])
        ->name('auth.register');
    Route::post('login', [AuthController::class, 'login'])
        ->name('auth.login');

    Route::post('admin', [AuthController::class, 'loginAdmin'])
        ->name('auth.admin');
});


Route::group(['prefix' => 'instance'], function () {
    Route::post('/', [InstanceController::class, 'create'])
        ->name('instance.create')
        ->middleware('auth:sanctum');
    Route::delete('/{instanceId}', [InstanceController::class, 'remove'])
        ->name('instance.remove')
        ->middleware('auth:sanctum');
});

Route::post('subscription', [SubscriptionController::class, 'save'])
    ->name('subscription.save')
    ->middleware('auth:sanctum');

Route::post('subscription/update', [SubscriptionController::class, 'update'])
    ->name('subscription.update')
    ->middleware('auth:sanctum');

Route::post('/host/update', [HostController::class, 'updateHostsConfig'])
    ->name('host.update')
    ->middleware('auth:sanctum');

Route::post('/host/delete', [HostController::class, 'deleteHostsConfig'])
    ->name('host.delete')
    ->middleware('auth:sanctum');

Route::get('/user/current', [UserController::class, 'current'])
    ->name('user.current')
    ->middleware('auth:sanctum');

Route::post('/user/resetPassword', [UserController::class, 'resetPassword'])
    ->name('user.reset-password');

Route::post('/user/changeResetPassword', [UserController::class, 'changeResetPassword'])
    ->name('user.change-reset-password');

Route::post('/host/check', [HostController::class, 'checkHostWork'])
    ->name('check.host');

Route::get('/cron', [CronController::class, 'start'])
    ->name('cron.start');

Route::post('/host/check/mail', [HostController::class, 'checkHostMail'])
    ->name('check.host.mail');

Route::post('/host/check/smtp', [HostController::class, 'checkHostSmtp'])
    ->name('check.host.smtp');

Route::post('/user/update', [UserController::class, 'update'])
    ->name('user.update')
    ->middleware('auth:sanctum');

Route::get('features', [FeatureController::class, 'list'])
    ->name('features.list')
    ->middleware('auth:sanctum');

Route::post('/webhook/success', [WebhookController::class, 'confirm'])
    ->name('webhook.success')
    ->middleware(WebhookMiddleware::class)
    ->withoutMiddleware(AllowOnlyJsonMiddleware::class);

Route::get('order', [OrderController::class, 'list'])
    ->name('order.list')
    ->middleware('auth:sanctum');

Route::get('history', [HistoryController::class, 'list'])
    ->name('history.list')
    ->middleware('auth:sanctum');

Route::get('/check-hosts', [HostController::class, 'checkHosts'])
    ->name('hosts.check')
    ->middleware('auth:sanctum');

Route::get('history/last', [HistoryController::class, 'last'])
    ->name('history.last')
    ->middleware('auth:sanctum');

Route::post('history', [HistoryController::class, 'save'])
    ->name('history.save')
    ->middleware('auth:sanctum');

Route::get('testAdminAuth', fn () => 'ok')
    ->middleware('auth:sanctum', RBAC::anyAbilityForRoute([PermissionEnum::TEST_ADMIN_PERMISSION]))
    ->name('testAdminAuth');

Route::middleware('auth:sanctum', RBAC::anyAbilityForRoute([PermissionEnum::TEST_ADMIN_PERMISSION]))->group(function () {
    Route::get('/users', [AdminUserController::class, 'index'])->name('admin.users');

    Route::get('/instances', [AdminInstanceController::class, 'index'])->name('admin.instances');

    Route::get('/instances/{field}', [AdminInstanceController::class, 'getData'])->name('instances.fields');

    Route::get('/orders', [AdminOrdersController::class, 'index'])->name('admin.orders');

    Route::get('/users/{field}', [AdminUserController::class, 'getData'])->name('user.fields');


    Route::get('/subscriptions', [AdminSubscriptionController::class, 'index'])->name('admin.subscriptions');

    Route::post('/clear-instances', [HostController::class, 'clearHosts'])->name('admin.clearInstances');

    Route::apiResource('/admin/features', AdminFeatureController::class);

    Route::apiResource('/admin/onesignal', AdminOneSignalController::class);

    Route::apiResource('/admin/hosts', AdminHostController::class);
});





//Route::get('ability', function () {
//    return 'ability';
//})->middleware(['auth:sanctum', RBAC::anyAbilityForRoute([PermissionEnum::LOGOUT])]);
//
//
//Route::get('abilities', function () {
//    return 'abilities';
//})->middleware(['auth:sanctum', RBAC::allAbilitiesForRoute([PermissionEnum::LOGOUT])]);

/**
 * but also everywhere you can use
 * $user->tokenCan(PermissionEnum::LOGOUT->value);
 *
 */
