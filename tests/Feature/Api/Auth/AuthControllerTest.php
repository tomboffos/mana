<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Auth;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\User\UserFactory;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Hashing\HashManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;

    use RefreshDatabase;

    public function testWithoutAuth()
    {
        $response = $this->getJson(route('auth.test'));
        $response->assertStatus(401);
    }

    public function testLogin()
    {
        $email = 'email@email.mail';
        $name = 'name';
        $password = 'pass12345';

        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'email' => $email,
            'name' => $name,
            'password' => $hasher->make($password)
        ]);

        $response = $this->postJson(route('auth.login'), [
            'email' => $email,
            'password' => $password
        ]);

        $response->assertOk();
        $response->assertJson([
            'result' => [
                'user' => [
                    'id' => $user->id,
                    'name' => $name,
                    'email' => $email,
                ],
            ],
            'error' => null,
        ]);
        $response->assertJsonStructure([
            'result' => [
                'token',
            ],
        ]);

        $token = $response->json()['result']['token'];

        $response = $this->getJson(route('auth.test'), [
            'Authorization' => 'Bearer ' . $token,
        ]);
        $response->assertOk();
    }

    public function testLoginWrongPassword()
    {
        $email = 'email@email.mail';
        $name = 'name';
        $password = 'pass12345';

        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        $company = $this->companyFactory->create();
        /** @var User $user */
        $this->userFactory->forCompany($company)->create([
            'email' => $email,
            'name' => $name,
            'password' => $hasher->make($password)
        ]);

        $response = $this->postJson(route('auth.login'), [
            'email' => $email,
            'password' => '123'
        ]);

        $response->assertStatus(401);
    }

    public function testRegister()
    {
        $email = 'email@email.mail';
        $name = 'name';
        $password = 'pass12345';

        $response = $this->postJson(route('auth.register'), [
            'email' => $email,
            'name' => $name,
            'password' => $password
        ]);

        $response->assertOk();
        $response->assertJson([
            'result' => [
                'user' => [
                    'name' => $name,
                    'email' => $email,
                ],
            ],
            'error' => null,
        ]);
        $response->assertJsonStructure([
            'result' => [
                'token',
            ],
        ]);

        $token = $response->json()['result']['token'];

        $response = $this->getJson(route('auth.test'), [
            'Authorization' => 'Bearer ' . $token,
        ]);
        $response->assertOk();
    }

    public function testChangePassword()
    {
        $email = 'email@email.mail';
        $name = 'name';
        $password = 'pass12345';
        $newPassword = 'pass1234567';
        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'email' => $email,
            'name' => $name,
            'password' => $hasher->make($password)
        ]);
        $token = $user->createToken('token');

        $response = $this->postJson(route('auth.changePassword'), [
            'old_password' => $password,
            'new_password' => $newPassword,
        ], [
            'Authorization' => 'Bearer ' . $token->plainTextToken,
        ]);

        $response->assertOk();
        $response->assertJson([
            'result' => [
                'id' => $user->id,
                'name' => $name,
                'email' => $email,
            ],
            'error' => null,
        ]);

        $response = $this->postJson(route('auth.login'), [
            'email' => $email,
            'password' => $newPassword
        ]);
        $response->assertOk();
    }
}
