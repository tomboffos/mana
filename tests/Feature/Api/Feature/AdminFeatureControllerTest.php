<?php

namespace Tests\Feature\Api\Feature;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\User\UserFactory;
use Domain\Feature\Models\Feature;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminFeatureControllerTest extends TestCase
{
    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;

    public function getToken()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::ADMIN
        ]);

        return $user->createToken('api-token');

    }

    public function test_list()
    {

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken()->plainTextToken,
        ])->getJson(route('features.index'),);

        $response->assertOk();
    }

    public function test_create()
    {

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken()->plainTextToken,
        ])->postJson(route('features.store'), [
            'alias' => 'key',
            'name' => 'name',
            'price' => '25',
            'active' => false
        ]);


        $response->assertStatus(201);
    }

    public function test_update()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken()->plainTextToken,
        ])->putJson(route('features.update', ['feature' => Feature::inRandomOrder()->first()->id]), [
            'alias' => 'key',
            'name' => 'name',
            'price' => '100',
            'active' => false
        ]);


        $response->assertOk();

    }

    public function test_delete()
    {
        exec('php artisan db:seed');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken()->plainTextToken,
        ])->deleteJson(route('features.destroy', ['feature' => Feature::inRandomOrder()->first()->id]),);

        $response->assertOk();
    }
}
