<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Feature;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\Feature\Services\FeatureService;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FeatureControllerTest extends TestCase
{
    use RefreshDatabase;

    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;

    public function testList()
    {

        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');

        $response = $this->getJson(route('features.list'), [
            'Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertOk();

    }
}
