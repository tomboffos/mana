<?php

namespace Tests\Feature\Api\Host;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HostControllerTest extends TestCase
{
    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;
    protected InstanceFactory $instanceFactory;
    protected HostFactory $hostFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function testHostClear()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::ADMIN
        ]);


        $token = $user->createToken('api-token');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token->plainTextToken,
        ])->postJson(route('admin.clearInstances'),);

        $response->assertOk();
    }


    public function testHostUpdate()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::ADMIN
        ]);

        $instance = $this->instanceFactory->forCompany($company)->create();

        $this->hostFactory->forInstance($instance)->create([
            'domain' => 'https://xmanax.com'
        ]);


        $this->subscriptionFactory->forInstance($instance)->create([
            'is_active' => 1
        ]);


        $token = $user->createToken('api-token');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token->plainTextToken
        ])->postJson(route('host.update'), [
            'url' => 'https://xmanax.com'
        ]);


        $response->assertOk();
    }

    public function testHostCheck()
    {
        try {
            $this->postJson(route('check.host'), [
                 'url' => 'xmanax.com'
            ]);
        } catch (\InvalidArgumentException $notExpected) {
            $this->fail();
        }
        $this->assertTrue(TRUE);
    }
}
