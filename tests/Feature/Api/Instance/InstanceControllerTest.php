<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Instance;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InstanceControllerTest extends TestCase
{
    use RefreshDatabase;

    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;
    protected HostFactory $hostFactory;
    protected InstanceFactory $instanceFactory;

    public function testCreate()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $this->hostFactory->withEmptyInstance()->create();

        $response = $this->postJson(route('instance.create'), [],
            ['Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertStatus(201);
        $response->assertJson([
            'result' => [
                'company_id' => $company->id,
            ],
            'error' => null,
        ]);

        $this->assertDatabaseHas('instances', [
            'company_id' => $company->id,
        ]);
        $this->assertDatabaseHas('hosts', [
            'instance_id' => $response->json('result')['id'],
        ]);
    }

    public function testCreateUnauthenticated()
    {
        $response = $this->postJson(route('instance.create'));
        $response->assertStatus(401);
    }

    public function testCreateNoHosts()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');

        $response = $this->postJson(route('instance.create'), [],
            ['Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertStatus(500);
        $response->assertJson([
            'error' => [
                'code' => 'no-available-hosts',
            ],
        ]);
    }

    public function testCreateAlreadyExists()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $this->hostFactory->withEmptyInstance()->create();
        $this->instanceFactory->forCompany($company)->create();

        $response = $this->postJson(route('instance.create'), [],
            ['Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertStatus(500);
        $response->assertJson([
            'error' => [
                'code' => 'instance-already-exists',
            ],
        ]);
    }
}
