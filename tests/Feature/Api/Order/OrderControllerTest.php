<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Order;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Order\OrderFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\Order\Models\Order;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    protected HostFactory $hostFactory;
    protected CompanyFactory $companyFactory;
    protected InstanceFactory $instanceFactory;
    protected UserFactory $userFactory;
    protected OrderFactory $orderFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function testList()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $instance = $this->instanceFactory->forCompany($company)->create();
        $this->hostFactory->forInstance($instance)->create();
        $subscription = $this->subscriptionFactory->forInstance($instance)->create();
        /** @var Order $order */
        $order = $this->orderFactory->forSubscription($subscription)->create();

        $response = $this->getJson(route('order.list'), ['Authorization' => 'Bearer ' . $token->plainTextToken]);
        $response->assertOk();
        $response->assertJson([
            'result' => [[
                'id' => $order->id,
                'subscription_id' => $order->subscription_id,
                'subscription' => [
                    'id' => $order->subscription->id,
                    'features' => json_decode($order->subscription->features, true),
                    'name' => $order->subscription->name,
                    'logo_path' => $order->subscription->logo_path,
                    'valid_from' => $order->subscription->valid_from,
                    'valid_until' => $order->subscription->valid_until,
                    'instance_id' => $order->subscription->instance_id,
                    'is_active' => $order->subscription->is_active,
                    'wallets' => $order->subscription->wallets != null ? json_decode($order->subscription->wallets, true) : ''
                ],
                'amount' => (int) $order->amount,
                'status' => $order->status->value,
                'external_invoice_id' => $order->external_invoice_id,
            ]],
            'error' => null
        ]);
    }
}
