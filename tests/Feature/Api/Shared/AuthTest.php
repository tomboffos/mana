<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Shared;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\User\UserFactory;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;

    public function testAdminAuth()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::ADMIN
        ]);
        $token = $user->createToken('token');

        $response = $this->getJson(route('testAdminAuth'), [
            'Authorization' => 'Bearer ' . $token->plainTextToken,
        ]);
        $response->assertOk();
    }

    public function testAdminAuthWithCustomer()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::CUSTOMER
        ]);
        $token = $user->createToken('token');

        $response = $this->getJson(route('testAdminAuth'), [
            'Authorization' => 'Bearer ' . $token->plainTextToken,
        ]);
        $response->assertStatus(403);
    }
}
