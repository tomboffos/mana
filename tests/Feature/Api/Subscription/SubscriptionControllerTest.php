<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Subscription;

use Carbon\Carbon;
use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\Feature\Services\FeatureService;
use Domain\Instance\Models\Instance;
use Domain\Order\Data\OrderStatusesEnum;
use Domain\Subscription\Models\Subscription;
use Domain\User\Data\RBAC;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscriptionControllerTest extends TestCase
{
    use RefreshDatabase;

    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;
    protected HostFactory $hostFactory;
    protected InstanceFactory $instanceFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function getToken()
    {
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create([
            'role_id' => RBAC::ADMIN
        ]);

        return $user->createToken('token');
    }

    public function testSave()
    {
        $features = array_keys(FeatureService::FEATURE_LIST);
        $name = 'name';
        $validFrom = Carbon::now();
        $validUntil = Carbon::now()->addMonth();

        $adminPassword = 'pass123456';

        $payUrl = 'https://pay.cryptocloud.plus/JWFHLOT1';
        $invoiceId = 'JWFHLOT1';
        $domain = $this->app['config']['services']['crypto_cloud']['domain'];
        /** @var Factory $http */
        $http = $this->app->make(Factory::class);
        $http->fake([
            $domain . '/v1/invoice/create' => $http->response([
                'status' => 'success',
                'pay_url' => $payUrl,
                'invoice_id' => $invoiceId,
            ]),
        ]);
        $this->app->instance(Factory::class, $http);

        Storage::fake('logos');
        $file = UploadedFile::fake()->image('logo.jpg');

        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        /** @var Instance $instance */
        $instance = $this->instanceFactory->forCompany($company)->create();
        $this->hostFactory->forInstance($instance);

        $response = $this->post(route('subscription.save'), [
            'features' => $features,
            'name' => $name,
            'logo' => $file,
            'valid_from' => $validFrom,
            'valid_until' => $validUntil,
            'admin_password' => $adminPassword,
            'external_domain' => 'vasya.com'
        ], [
            'Authorization' => 'Bearer ' . $token->plainTextToken,
            'Accept' => 'application/json',
        ]);

        $response->assertOk();
        // $response->assertJson([
        //     'result' => [
        //         'subscription' => [
        //             'features' => $features,
        //             'name' => $name,
        //             'logo_path' => 'logos/' . $file->hashName(),
        //             'instance_id' => $instance->id,
        //         ],
        //         'order' => [
        //             'amount' => 250 || 225 || 0,
        //             'status' => OrderStatusesEnum::CREATED->value,
        //             'external_invoice_id' => $invoiceId,
        //         ],
        //     ],
        //     'error' => null,
        // ]);

        $this->assertTrue(isset($response->json()['result']['subscription']['valid_until']));
        $this->assertTrue($validUntil->equalTo(Carbon::parse($response->json()['result']['subscription']['valid_until'])));
    }


    public function testUpdate()
    {
        $features = array_keys(FeatureService::FEATURE_LIST);
        $name = 'name';
        $company = $this->companyFactory->create();
        $instance = $this->instanceFactory->forCompany($company)->create();
        $this->hostFactory->forInstance($instance)->create([
            'domain' => 'https://s2.xmanax.com'
        ]);

        $this->subscriptionFactory->forInstance($instance)->create([
            'is_active' => 1
        ]);
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $validFrom = Carbon::now();
        $validUntil = Carbon::now()->addMonth();
        $adminPassword = 'pass123456';

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token->plainTextToken,
            'Accept' => 'application/json',
        ])->postJson(route('subscription.update', [
            'features' => $features,
            'name' => $name,
            'logo' => null,
            'valid_from' => $validFrom->toDateString(),
            'valid_until' => $validUntil->toDateString(),
            'admin_password' => $adminPassword,
        ]));
        $response->assertOk();
    }

    public function testCheckSubscription()
    {
        $response = $this->getJson(
            route('subscription.check'),
            ['Accept' => 'application/json',]
        );
        $response->assertOk();
    }
}
