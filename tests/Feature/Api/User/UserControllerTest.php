<?php

declare(strict_types=1);

namespace Tests\Feature\Api\User;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\Instance\Models\Instance;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    protected CompanyFactory $companyFactory;
    protected UserFactory $userFactory;
    protected HostFactory $hostFactory;
    protected InstanceFactory $instanceFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function testCurrent()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $this->hostFactory->withEmptyInstance()->create();
        /** @var Instance $instance */
        $instance = $this->instanceFactory->forCompany($company)->create();
        $this->subscriptionFactory->forInstance($instance)->count(2)->create();

        $response = $this->getJson(route('user.current'),
            ['Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertOk();
        $response->assertJson([
            'result' => [
                'id' => $user->id,
                'instance' => [
                    'id' => $instance->id,
                    'company_id' => $instance->company_id,
                    'subscriptions' => [
                        [
                            'id' => $instance->subscriptions->first()->id,
                            'features' => json_decode($instance->subscriptions->first()->features, true),
                            'name' => $instance->subscriptions->first()->name,
                            'logo_path' => $instance->subscriptions->first()->logo_path,
                            'valid_until' => $instance->subscriptions->first()->valid_until,
                            'is_active' => $instance->subscriptions->first()->is_active,
                        ],
                        [
                            'id' => $instance->subscriptions->last()->id,
                            'features' => json_decode($instance->subscriptions->last()->features, true),
                            'name' => $instance->subscriptions->last()->name,
                            'logo_path' => $instance->subscriptions->last()->logo_path,
                            'valid_until' => $instance->subscriptions->last()->valid_until,
                            'is_active' => $instance->subscriptions->last()->is_active,
                        ],
                    ],
                ],
            ],
            'error' => null,
        ]);
    }

    public function testUpdate()
    {
        $name = 'name';
        $email = 'email@mail.mail';

        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var User $user */
        $user = $this->userFactory->forCompany($company)->create();
        $token = $user->createToken('token');
        $this->hostFactory->withEmptyInstance()->create();
        /** @var Instance $instance */
        $instance = $this->instanceFactory->forCompany($company)->create();
        $this->subscriptionFactory->forInstance($instance)->count(2)->create();

        $response = $this->postJson(route('user.update'), [
            'name' => $name,
            'email' => $email,
        ],
            ['Authorization' => 'Bearer ' . $token->plainTextToken]
        );

        $response->assertOk();
        $response->assertJson([
            'result' => [
                'id' => $user->id,
                'name' => $name,
                'email' => $email,
            ],
            'error' => null,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $name,
            'email' => $email,
        ]);
    }
}
