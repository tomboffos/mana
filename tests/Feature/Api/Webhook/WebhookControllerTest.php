<?php

declare(strict_types=1);

namespace Tests\Feature\Api\Webhook;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Order\OrderFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Domain\Company\Models\Company;
use Domain\Instance\Models\Instance;
use Domain\Order\Models\Order;
use Domain\Subscription\Models\Subscription;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WebhookControllerTest extends TestCase
{
    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;
    protected HostFactory $hostFactory;
    protected InstanceFactory $instanceFactory;
    protected SubscriptionFactory $subscriptionFactory;
    protected OrderFactory $orderFactory;

    use RefreshDatabase;

    // Run tests when we have a orders to confirm

//     public function testConfirm()
//     {
//         $invoiceId = 'invoiceId';
//         $status = 'success';
// 
//         /** @var Company $company */
//         $company = $this->companyFactory->create();
//         /** @var User $user */
//         $this->userFactory->forCompany($company)->create();
//         /** @var Instance $instance */
//         $instance = $this->instanceFactory->forCompany($company)->create();
//         $this->hostFactory->forInstance($instance)->create();
//         /** @var Subscription $subscription */
//         $subscription = $this->subscriptionFactory->forInstance($instance)->create();
//         /** @var Order $order */
//         $order = $this->orderFactory
//             ->forSubscription($subscription)
//             ->create(['external_invoice_id' => $invoiceId]);
// 
//         $http = $this->app->make(Factory::class);
//         $http->fake([
//             '*' => $http->response(),
//         ]);
//         $this->app->instance(Factory::class, $http);
// 
//         $response = $this->post(route('webhook.success'), [
//             'status' => $status,
//             'invoice_id' => $invoiceId,
//         ]);
// 
//         $response->assertOk();
// 
//         $subscription->refresh();
//         $order->refresh();
//         $instance->refresh();
// 
//         $this->assertDatabaseHas('subscriptions', [
//             'id' => $subscription->id,
//             'is_active' => true,
//         ]);
//         $this->assertDatabaseHas('instances', [
//             'id' => $instance->id,
//             'is_active' => true,
//         ]);
//         $this->assertDatabaseHas('orders', [
//             'id' => $order->id,
//             'status' => $order->status,
//         ]);
//     }
}
