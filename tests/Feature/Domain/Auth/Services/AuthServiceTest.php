<?php

declare(strict_types=1);

namespace Tests\Feature\Domain\Auth\Services;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\User\UserFactory;
use Domain\Auth\Data\LoginUserDto;
use Domain\Auth\Data\RegisterUserDto;
use Domain\Auth\Exceptions\UserAlreadyExistsException;
use Domain\Auth\Exceptions\WrongPasswordException;
use Domain\Auth\Services\AuthService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Hashing\HashManager;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Throwable;

class AuthServiceTest extends TestCase
{
    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;

    use RefreshDatabase;

    /**
     * @throws Throwable
     */
    public function testRegister()
    {
        $name = 'name';
        $email = 'email';
        $password = 'pass122345';

        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        /** @var AuthService $service */
        $service = $this->app->make(AuthService::class);

        $result = $service->register(new RegisterUserDto(
            $name,
            $email,
            $password
        ));

        $this->assertDatabaseHas('users', [
            'name' => $name,
            'email' => $email,
        ]);

        $this->assertTrue(
            $hasher->check($password, $result->user->password)
        );
    }

    /**
     * @throws Throwable
     */
    public function testRegisterUserExists()
    {
        $name = 'name';
        $email = 'email@email.mail';
        $password = '123456780';

        $company = $this->companyFactory->create();
        $this->userFactory->forCompany($company)->create([
            'email' => $email,
        ]);

        /** @var AuthService $service */
        $service = $this->app->make(AuthService::class);

        $this->expectException(UserAlreadyExistsException::class);

        $service->register(new RegisterUserDto(
            $name,
            $email,
            $password
        ));
    }

    public function testLogin()
    {
        $name = 'name';
        $email = 'email@email.mail';
        $password = '123456780';

        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        $company = $this->companyFactory->create();
        $user = $this->userFactory->forCompany($company)->create([
            'name' => $name,
            'email' => $email,
            'password' => $hasher->make($password),
        ]);

        /** @var AuthService $service */
        $service = $this->app->make(AuthService::class);
        $result = $service->login(new LoginUserDto($email, $password));

        $user->refresh();

        $this->assertEquals($user->attributesToArray(), $result->user->attributesToArray());
    }

    public function testLoginUnsuccessfully()
    {
        $name = 'name';
        $email = 'email@email.mail';
        $password = '123456780';
        $wrongPassword = '1234';

        /** @var HashManager $hasher */
        $hasher = $this->app->make(HashManager::class);

        $company = $this->companyFactory->create();
        $this->userFactory->forCompany($company)->create([
            'name' => $name,
            'email' => $email,
            'password' => $hasher->make($password),
        ]);

        $this->expectException(WrongPasswordException::class);

        /** @var AuthService $service */
        $service = $this->app->make(AuthService::class);
        $service->login(new LoginUserDto($email, $wrongPassword));
    }
}
