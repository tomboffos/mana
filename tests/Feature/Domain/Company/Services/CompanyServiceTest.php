<?php

declare(strict_types=1);

namespace Tests\Feature\Domain\Company\Services;

use Domain\Company\Services\CompanyService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CompanyServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testCreate()
    {
        /** @var CompanyService $service */
        $service = $this->app->make(CompanyService::class);
        $company = $service->create();

        $this->assertDatabaseHas('companies', [
            'id' => $company->id,
        ]);
    }
}
