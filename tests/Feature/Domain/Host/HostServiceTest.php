<?php

declare(strict_types=1);

namespace Tests\Feature\Domain\Host;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Domain\Host\Exceptions\NoAvailableHostsException;
use Domain\Host\Models\Host;
use Domain\Host\Services\HostService;
use Domain\Instance\Models\Instance;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HostServiceTest extends TestCase
{
    use RefreshDatabase;

    protected CompanyFactory $companyFactory;
    protected InstanceFactory $instanceFactory;
    protected HostFactory $hostFactory;

    public function testAttachAvailableToInstance()
    {
        $company = $this->companyFactory->create();
        /** @var Instance $instance */
        $instance = $this->instanceFactory->forCompany($company)->create();
        /** @var Host $host */
        $host = $this->hostFactory->create([
            'instance_id' => null,
        ]);

        /** @var HostService $service */
        $service = $this->app->make(HostService::class);
        $service->attachAvailableToInstance($instance);

        $host->refresh();

        $this->assertEquals($instance->id, $host->instance_id);
        $this->assertDatabaseHas('hosts', [
            'instance_id' => $instance->id,
        ]);
    }

    public function testAttachAvailableToInstanceNoHosts()
    {
        $this->expectException(NoAvailableHostsException::class);

        $company = $this->companyFactory->create();
        /** @var Instance $instance */
        $instance = $this->instanceFactory->forCompany($company)->create();

        /** @var HostService $service */
        $service = $this->app->make(HostService::class);
        $service->attachAvailableToInstance($instance);
    }
}
