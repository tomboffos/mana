<?php

declare(strict_types=1);

namespace Tests\Feature\Domain\Instance;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Host\HostFactory;
use Database\Factories\Instance\InstanceFactory;
use Domain\Company\Models\Company;
use Domain\Host\Exceptions\NoAvailableHostsException;
use Domain\Host\Models\Host;
use Domain\Instance\Data\CreateInstanceDto;
use Domain\Instance\Exceptions\InstanceAlreadyExistsException;
use Domain\Instance\Services\InstanceService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Throwable;

class InstanceServiceTest extends TestCase
{
    use RefreshDatabase;

    protected CompanyFactory $companyFactory;
    protected HostFactory $hostFactory;
    protected InstanceFactory $instanceFactory;

    /**
     * @throws Throwable
     */
    public function testCreate()
    {
        /** @var Company $company */
        $company = $this->companyFactory->create();
        /** @var Host $host */
        $host = $this->hostFactory->withEmptyInstance()->create();

        /** @var InstanceService $service */
        $service = $this->app->make(InstanceService::class);
        $instance = $service->create(new CreateInstanceDto($company->id));

        $host->refresh();

        $this->assertEquals($company->id, $instance->company_id);
        $this->assertEquals($instance->id, $host->instance_id);

        $this->assertDatabaseHas('instances', [
            'company_id' => $company->id,
        ]);
        $this->assertDatabaseHas('hosts', [
           'instance_id' => $instance->id,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function testCreateNoHosts()
    {
        $this->expectException(NoAvailableHostsException::class);

        /** @var Company $company */
        $company = $this->companyFactory->create();

        /** @var InstanceService $service */
        $service = $this->app->make(InstanceService::class);
        $service->create(new CreateInstanceDto($company->id));
    }

    /**
     * @throws Throwable
     */
    public function testCreateAlreadyExists()
    {
        $this->expectException(InstanceAlreadyExistsException::class);

        /** @var Company $company */
        $company = $this->companyFactory->create();
        $this->instanceFactory->forCompany($company)->create();

        /** @var InstanceService $service */
        $service = $this->app->make(InstanceService::class);
        $service->create(new CreateInstanceDto($company->id));
    }
}
