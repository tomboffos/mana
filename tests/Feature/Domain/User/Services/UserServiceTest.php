<?php

declare(strict_types=1);

namespace Tests\Feature\Domain\User\Services;

use Database\Factories\Company\CompanyFactory;
use Domain\Company\Models\Company;
use Domain\User\Data\CreateUserDto;
use Domain\User\Data\RBAC;
use Domain\User\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserServiceTest extends TestCase
{
    protected CompanyFactory $companyFactory;

    use RefreshDatabase;

    public function testCreate()
    {
        $name = 'name';
        $email = 'email';
        $password = 'password';

        /** @var Company $company */
        $company = $this->companyFactory->create();

        /** @var UserService $service */
        $service = $this->app->make(UserService::class);
        $service->create(new CreateUserDto(
            $name,
            $email,
            $password,
            $company->id,
            RBAC::CUSTOMER
        ));

        $this->assertDatabaseHas('users', [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'company_id' => $company->id,
        ]);
    }
}
