<?php

declare(strict_types=1);

namespace Tests\Feature\Infrastructure\Gateways\CryptoCloud;

use Illuminate\Http\Client\Factory;
use Infrastructure\Gateways\CryptoCloud\CryptoCloudGateway;
use Infrastructure\Gateways\CryptoCloud\Exceptions\CryptoCloudGatewayException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CryptoCloudGatewayTest extends TestCase
{
    /**
     * @throws CryptoCloudGatewayException
     */
    public function testCreateInvoice()
    {
        $payUrl = 'https://pay.cryptocloud.plus/JWFHLOT1';
        $invoiceId = 'JWFHLOT1';
        $amount = 199.99;

        $domain = $this->app['config']['services']['crypto_cloud']['domain'];
        /** @var Factory $http */
        $http = $this->app->make(Factory::class);
        $http->fake([
            $domain . '/v1/invoice/create' => $http->response([
                'status' => 'success',
                'pay_url' => $payUrl,
                'invoice_id' => $invoiceId,
            ]),
        ]);
        $this->app->instance(Factory::class, $http);

        /** @var CryptoCloudGateway $gateway */
        $gateway = $this->app->make(CryptoCloudGateway::class);
        $invoice = $gateway->createInvoice($amount);

        $this->assertEquals($payUrl, $invoice->payUrl);
        $this->assertEquals($invoiceId, $invoice->invoiceId);
    }
}
