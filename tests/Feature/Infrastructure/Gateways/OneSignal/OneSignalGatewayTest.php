<?php

declare(strict_types=1);

namespace Tests\Feature\Infrastructure\Gateways\OneSignal;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Domain\ServicesCreateWithHost\Services\ServicesCreateWithHost;
use Database\Factories\Host\HostFactory;

class OneSignalGatewayTest extends TestCase
{
    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;
    protected InstanceFactory $instanceFactory;
    protected HostFactory $hostFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function testCreateOneSignalApp()
    {
        try {

            $company = $this->companyFactory->create();
            $instance = $this->instanceFactory->forCompany($company)->create([
                'name' => 'TestIntstance'
            ]);

            $this->hostFactory->forInstance($instance)->create([
                'domain' => 'https://s1.xmanax.com'
            ]);

            $this->subscriptionFactory->forInstance($instance)->create([
                'is_active' => 1
            ]);

            $service = $this->app->make(ServicesCreateWithHost::class);
            $service->createOneSignalApp($instance);
        } catch (\InvalidArgumentException $notExpected) {
            $this->fail();
        }
        $this->assertTrue(TRUE);
    }
}
