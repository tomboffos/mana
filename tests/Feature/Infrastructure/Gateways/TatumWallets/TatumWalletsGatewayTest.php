<?php

declare(strict_types=1);

namespace Tests\Feature\Infrastructure\Gateways\TatumWallets;

use Database\Factories\Company\CompanyFactory;
use Database\Factories\Instance\InstanceFactory;
use Database\Factories\Subscription\SubscriptionFactory;
use Database\Factories\User\UserFactory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Domain\ServicesCreateWithHost\Services\ServicesCreateWithHost;
use Database\Factories\Host\HostFactory;

class TatumWalletsGatewayTest extends TestCase
{
    protected UserFactory $userFactory;
    protected CompanyFactory $companyFactory;
    protected InstanceFactory $instanceFactory;
    protected HostFactory $hostFactory;
    protected SubscriptionFactory $subscriptionFactory;

    public function testCreateTatumWallets()
    {
        try {
            $company = $this->companyFactory->create();
            $service = $this->app->make(ServicesCreateWithHost::class);
            $instance = $this->instanceFactory->forCompany($company)->create([
                'name' => 'Test Intstance'
            ]);

            $this->hostFactory->forInstance($instance)->create([
                'domain' => 'https://s3.xmanax.com'
            ]);

            $this->subscriptionFactory->forInstance($instance)->create([
                'is_active' => 1
            ]);
            $service->createTatumWallets('["tron","bitcoin","60457cac-2d8a-43e6-9415-8e76abe6b45f","{\"tron\":\"16\",\"bitcoin\":\"0.00043\"}"]', $instance);
        } catch (\InvalidArgumentException $notExpected) {
            $this->fail();
        }
        $this->assertTrue(TRUE);
    }
}
