<?php

namespace Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use ReflectionClass;
use ReflectionProperty;
use RonasIT\Support\AutoDoc\Tests\AutoDocTestCaseTrait;

abstract class TestCase extends BaseTestCase
{
    use AutoDocTestCaseTrait;
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setFactories();
    }

    protected function setFactories(): void
    {
        $reflect = new ReflectionClass($this);
        foreach ($reflect->getProperties(ReflectionProperty::IS_PROTECTED) as $property) {
            if (
                $property->hasType() &&
                get_parent_class($property->getType()->getName()) === Factory::class
            ) {
                $propertyName = $property->getName();
                $this->$propertyName = $this->app->make($property->getType()->getName());
            }
        }
    }
}


